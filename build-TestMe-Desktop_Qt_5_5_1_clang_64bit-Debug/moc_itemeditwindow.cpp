/****************************************************************************
** Meta object code from reading C++ file 'itemeditwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../TestMe/Window/Group/EditGroup/ItemEditWindow/itemeditwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'itemeditwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ItemEditWindow_t {
    QByteArrayData data[10];
    char stringdata0[122];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ItemEditWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ItemEditWindow_t qt_meta_stringdata_ItemEditWindow = {
    {
QT_MOC_LITERAL(0, 0, 14), // "ItemEditWindow"
QT_MOC_LITERAL(1, 15, 19), // "deleteButtonClicked"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 14), // "deleteClientID"
QT_MOC_LITERAL(4, 51, 6), // "status"
QT_MOC_LITERAL(5, 58, 11), // "UserObject*"
QT_MOC_LITERAL(6, 70, 3), // "usr"
QT_MOC_LITERAL(7, 74, 10), // "statusFlag"
QT_MOC_LITERAL(8, 85, 12), // "userPassword"
QT_MOC_LITERAL(9, 98, 23) // "on_deleteButton_clicked"

    },
    "ItemEditWindow\0deleteButtonClicked\0\0"
    "deleteClientID\0status\0UserObject*\0usr\0"
    "statusFlag\0userPassword\0on_deleteButton_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ItemEditWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    3,   42,    2, 0x0a /* Public */,
       4,    2,   49,    2, 0x2a /* Public | MethodCloned */,
       4,    1,   54,    2, 0x2a /* Public | MethodCloned */,
       9,    0,   57,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 5, QMetaType::Bool, QMetaType::QString,    6,    7,    8,
    QMetaType::Void, 0x80000000 | 5, QMetaType::Bool,    6,    7,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void,

       0        // eod
};

void ItemEditWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ItemEditWindow *_t = static_cast<ItemEditWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->deleteButtonClicked((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->status((*reinterpret_cast< UserObject*(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3]))); break;
        case 2: _t->status((*reinterpret_cast< UserObject*(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 3: _t->status((*reinterpret_cast< UserObject*(*)>(_a[1]))); break;
        case 4: _t->on_deleteButton_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (ItemEditWindow::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ItemEditWindow::deleteButtonClicked)) {
                *result = 0;
            }
        }
    }
}

const QMetaObject ItemEditWindow::staticMetaObject = {
    { &QFrame::staticMetaObject, qt_meta_stringdata_ItemEditWindow.data,
      qt_meta_data_ItemEditWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ItemEditWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ItemEditWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ItemEditWindow.stringdata0))
        return static_cast<void*>(const_cast< ItemEditWindow*>(this));
    return QFrame::qt_metacast(_clname);
}

int ItemEditWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QFrame::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void ItemEditWindow::deleteButtonClicked(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
