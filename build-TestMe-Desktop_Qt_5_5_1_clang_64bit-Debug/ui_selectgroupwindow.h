/********************************************************************************
** Form generated from reading UI file 'selectgroupwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SELECTGROUPWINDOW_H
#define UI_SELECTGROUPWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SelectGroupWindow
{
public:
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *selectGroupLayout;
    QVBoxLayout *scrollAreaLayout;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *itemsGroupLayout;
    QSpacerItem *verticalSpacer;
    QPushButton *addPushButton;

    void setupUi(QFrame *SelectGroupWindow)
    {
        if (SelectGroupWindow->objectName().isEmpty())
            SelectGroupWindow->setObjectName(QStringLiteral("SelectGroupWindow"));
        SelectGroupWindow->resize(349, 286);
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(SelectGroupWindow->sizePolicy().hasHeightForWidth());
        SelectGroupWindow->setSizePolicy(sizePolicy);
        SelectGroupWindow->setMinimumSize(QSize(330, 96));
        SelectGroupWindow->setFrameShape(QFrame::StyledPanel);
        SelectGroupWindow->setFrameShadow(QFrame::Raised);
        horizontalLayout_2 = new QHBoxLayout(SelectGroupWindow);
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setSizeConstraint(QLayout::SetDefaultConstraint);
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        selectGroupLayout = new QVBoxLayout();
        selectGroupLayout->setSpacing(0);
        selectGroupLayout->setObjectName(QStringLiteral("selectGroupLayout"));
        selectGroupLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        selectGroupLayout->setContentsMargins(-1, -1, -1, 0);
        scrollAreaLayout = new QVBoxLayout();
        scrollAreaLayout->setSpacing(0);
        scrollAreaLayout->setObjectName(QStringLiteral("scrollAreaLayout"));
        scrollAreaLayout->setSizeConstraint(QLayout::SetMaximumSize);
        scrollAreaLayout->setContentsMargins(-1, -1, -1, 0);
        scrollArea = new QScrollArea(SelectGroupWindow);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(scrollArea->sizePolicy().hasHeightForWidth());
        scrollArea->setSizePolicy(sizePolicy1);
        scrollArea->setMinimumSize(QSize(320, 0));
        scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        scrollArea->setWidgetResizable(true);
        scrollArea->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 341, 254));
        sizePolicy1.setHeightForWidth(scrollAreaWidgetContents->sizePolicy().hasHeightForWidth());
        scrollAreaWidgetContents->setSizePolicy(sizePolicy1);
        verticalLayout_2 = new QVBoxLayout(scrollAreaWidgetContents);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setSizeConstraint(QLayout::SetDefaultConstraint);
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        itemsGroupLayout = new QVBoxLayout();
        itemsGroupLayout->setSpacing(5);
        itemsGroupLayout->setObjectName(QStringLiteral("itemsGroupLayout"));
        itemsGroupLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        itemsGroupLayout->setContentsMargins(5, 3, 5, 3);
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        itemsGroupLayout->addItem(verticalSpacer);


        verticalLayout_2->addLayout(itemsGroupLayout);

        scrollArea->setWidget(scrollAreaWidgetContents);

        scrollAreaLayout->addWidget(scrollArea);

        addPushButton = new QPushButton(SelectGroupWindow);
        addPushButton->setObjectName(QStringLiteral("addPushButton"));
        QIcon icon;
        icon.addFile(QStringLiteral(":/icon/editGroupIcon"), QSize(), QIcon::Normal, QIcon::On);
        addPushButton->setIcon(icon);

        scrollAreaLayout->addWidget(addPushButton);


        selectGroupLayout->addLayout(scrollAreaLayout);


        horizontalLayout_2->addLayout(selectGroupLayout);


        retranslateUi(SelectGroupWindow);

        QMetaObject::connectSlotsByName(SelectGroupWindow);
    } // setupUi

    void retranslateUi(QFrame *SelectGroupWindow)
    {
        SelectGroupWindow->setWindowTitle(QApplication::translate("SelectGroupWindow", "Select Group", 0));
        addPushButton->setText(QApplication::translate("SelectGroupWindow", "Add group", 0));
    } // retranslateUi

};

namespace Ui {
    class SelectGroupWindow: public Ui_SelectGroupWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SELECTGROUPWINDOW_H
