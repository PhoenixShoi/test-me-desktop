/********************************************************************************
** Form generated from reading UI file 'settingwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETTINGWINDOW_H
#define UI_SETTINGWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SettingWindow
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *settingsLayout;
    QHBoxLayout *selectItemLayout;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QVBoxLayout *verticalLayout_2;
    QPushButton *generalButton;
    QPushButton *networkButton;
    QSpacerItem *verticalSpacer;
    QPushButton *defaultSettingsButton;
    QVBoxLayout *settingLayout;
    QStackedWidget *stackedWidget;
    QWidget *generalPage;
    QVBoxLayout *verticalLayout_4;
    QGroupBox *startingGroupBox;
    QVBoxLayout *verticalLayout_5;
    QCheckBox *splashScreenCheckBox;
    QLabel *applicationIconLabel;
    QComboBox *applicationIconComboBox;
    QSpacerItem *generalPageVerticalSpacer;
    QWidget *networkPage;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *portVerticalLayout;
    QLabel *portLabel;
    QLineEdit *portLineEdit;
    QSpacerItem *networkPageVerticalSpacer;

    void setupUi(QFrame *SettingWindow)
    {
        if (SettingWindow->objectName().isEmpty())
            SettingWindow->setObjectName(QStringLiteral("SettingWindow"));
        SettingWindow->resize(581, 528);
        SettingWindow->setFrameShape(QFrame::StyledPanel);
        SettingWindow->setFrameShadow(QFrame::Raised);
        verticalLayout = new QVBoxLayout(SettingWindow);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        settingsLayout = new QHBoxLayout();
        settingsLayout->setSpacing(0);
        settingsLayout->setObjectName(QStringLiteral("settingsLayout"));
        settingsLayout->setContentsMargins(-1, -1, 0, -1);
        selectItemLayout = new QHBoxLayout();
        selectItemLayout->setObjectName(QStringLiteral("selectItemLayout"));
        scrollArea = new QScrollArea(SettingWindow);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setMinimumSize(QSize(200, 200));
        scrollArea->setMaximumSize(QSize(200, 16777215));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 198, 520));
        verticalLayout_2 = new QVBoxLayout(scrollAreaWidgetContents);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        generalButton = new QPushButton(scrollAreaWidgetContents);
        generalButton->setObjectName(QStringLiteral("generalButton"));
        QIcon icon;
        icon.addFile(QStringLiteral(":/icon/generalIcon"), QSize(), QIcon::Normal, QIcon::On);
        generalButton->setIcon(icon);

        verticalLayout_2->addWidget(generalButton);

        networkButton = new QPushButton(scrollAreaWidgetContents);
        networkButton->setObjectName(QStringLiteral("networkButton"));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/icon/networkIcon"), QSize(), QIcon::Normal, QIcon::On);
        networkButton->setIcon(icon1);

        verticalLayout_2->addWidget(networkButton);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        defaultSettingsButton = new QPushButton(scrollAreaWidgetContents);
        defaultSettingsButton->setObjectName(QStringLiteral("defaultSettingsButton"));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/icon/defaultSettingsIcon"), QSize(), QIcon::Normal, QIcon::On);
        defaultSettingsButton->setIcon(icon2);

        verticalLayout_2->addWidget(defaultSettingsButton);

        scrollArea->setWidget(scrollAreaWidgetContents);

        selectItemLayout->addWidget(scrollArea);


        settingsLayout->addLayout(selectItemLayout);

        settingLayout = new QVBoxLayout();
        settingLayout->setSpacing(0);
        settingLayout->setObjectName(QStringLiteral("settingLayout"));
        settingLayout->setContentsMargins(-1, -1, 0, -1);
        stackedWidget = new QStackedWidget(SettingWindow);
        stackedWidget->setObjectName(QStringLiteral("stackedWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(stackedWidget->sizePolicy().hasHeightForWidth());
        stackedWidget->setSizePolicy(sizePolicy);
        generalPage = new QWidget();
        generalPage->setObjectName(QStringLiteral("generalPage"));
        verticalLayout_4 = new QVBoxLayout(generalPage);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        startingGroupBox = new QGroupBox(generalPage);
        startingGroupBox->setObjectName(QStringLiteral("startingGroupBox"));
        verticalLayout_5 = new QVBoxLayout(startingGroupBox);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        splashScreenCheckBox = new QCheckBox(startingGroupBox);
        splashScreenCheckBox->setObjectName(QStringLiteral("splashScreenCheckBox"));
        splashScreenCheckBox->setChecked(false);

        verticalLayout_5->addWidget(splashScreenCheckBox);

        applicationIconLabel = new QLabel(startingGroupBox);
        applicationIconLabel->setObjectName(QStringLiteral("applicationIconLabel"));

        verticalLayout_5->addWidget(applicationIconLabel);

        applicationIconComboBox = new QComboBox(startingGroupBox);
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/icon/testMeStyle1Icon"), QSize(), QIcon::Normal, QIcon::On);
        applicationIconComboBox->addItem(icon3, QString());
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/icon/testMeStyle2Icon"), QSize(), QIcon::Normal, QIcon::On);
        applicationIconComboBox->addItem(icon4, QString());
        QIcon icon5;
        icon5.addFile(QStringLiteral(":/icon/testMeStyle3Icon"), QSize(), QIcon::Normal, QIcon::On);
        applicationIconComboBox->addItem(icon5, QString());
        QIcon icon6;
        icon6.addFile(QStringLiteral(":/icon/testMeStyle4Icon"), QSize(), QIcon::Normal, QIcon::On);
        applicationIconComboBox->addItem(icon6, QString());
        applicationIconComboBox->setObjectName(QStringLiteral("applicationIconComboBox"));

        verticalLayout_5->addWidget(applicationIconComboBox);


        verticalLayout_4->addWidget(startingGroupBox);

        generalPageVerticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(generalPageVerticalSpacer);

        stackedWidget->addWidget(generalPage);
        networkPage = new QWidget();
        networkPage->setObjectName(QStringLiteral("networkPage"));
        verticalLayout_3 = new QVBoxLayout(networkPage);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        portVerticalLayout = new QVBoxLayout();
        portVerticalLayout->setObjectName(QStringLiteral("portVerticalLayout"));
        portLabel = new QLabel(networkPage);
        portLabel->setObjectName(QStringLiteral("portLabel"));

        portVerticalLayout->addWidget(portLabel);

        portLineEdit = new QLineEdit(networkPage);
        portLineEdit->setObjectName(QStringLiteral("portLineEdit"));

        portVerticalLayout->addWidget(portLineEdit);


        verticalLayout_3->addLayout(portVerticalLayout);

        networkPageVerticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(networkPageVerticalSpacer);

        stackedWidget->addWidget(networkPage);

        settingLayout->addWidget(stackedWidget);


        settingsLayout->addLayout(settingLayout);


        verticalLayout->addLayout(settingsLayout);


        retranslateUi(SettingWindow);

        stackedWidget->setCurrentIndex(0);
        applicationIconComboBox->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(SettingWindow);
    } // setupUi

    void retranslateUi(QFrame *SettingWindow)
    {
        SettingWindow->setWindowTitle(QApplication::translate("SettingWindow", "Settings", 0));
        generalButton->setText(QApplication::translate("SettingWindow", "General", 0));
        networkButton->setText(QApplication::translate("SettingWindow", "Network", 0));
        defaultSettingsButton->setText(QApplication::translate("SettingWindow", "Default settings", 0));
        startingGroupBox->setTitle(QApplication::translate("SettingWindow", "Starting programm", 0));
        splashScreenCheckBox->setText(QApplication::translate("SettingWindow", "Enable splashscreen", 0));
        applicationIconLabel->setText(QApplication::translate("SettingWindow", "Style application icon", 0));
        applicationIconComboBox->setItemText(0, QApplication::translate("SettingWindow", "Style 1", 0));
        applicationIconComboBox->setItemText(1, QApplication::translate("SettingWindow", "Style 2", 0));
        applicationIconComboBox->setItemText(2, QApplication::translate("SettingWindow", "Style 3", 0));
        applicationIconComboBox->setItemText(3, QApplication::translate("SettingWindow", "Style 4", 0));

        portLabel->setText(QApplication::translate("SettingWindow", "Port", 0));
    } // retranslateUi

};

namespace Ui {
    class SettingWindow: public Ui_SettingWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETTINGWINDOW_H
