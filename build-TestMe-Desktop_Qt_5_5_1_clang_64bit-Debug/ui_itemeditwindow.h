/********************************************************************************
** Form generated from reading UI file 'itemeditwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ITEMEDITWINDOW_H
#define UI_ITEMEDITWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ItemEditWindow
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *itemLayout;
    QVBoxLayout *statusIconLayout;
    QLabel *statusLabel;
    QVBoxLayout *infoLayout;
    QLabel *infoLabel;
    QVBoxLayout *deleteButtonLayout;
    QPushButton *deleteButton;

    void setupUi(QFrame *ItemEditWindow)
    {
        if (ItemEditWindow->objectName().isEmpty())
            ItemEditWindow->setObjectName(QStringLiteral("ItemEditWindow"));
        ItemEditWindow->resize(230, 32);
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(ItemEditWindow->sizePolicy().hasHeightForWidth());
        ItemEditWindow->setSizePolicy(sizePolicy);
        ItemEditWindow->setMinimumSize(QSize(230, 32));
        ItemEditWindow->setMaximumSize(QSize(999999, 36));
        ItemEditWindow->setFrameShape(QFrame::StyledPanel);
        ItemEditWindow->setFrameShadow(QFrame::Raised);
        verticalLayout = new QVBoxLayout(ItemEditWindow);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        itemLayout = new QHBoxLayout();
        itemLayout->setSpacing(0);
        itemLayout->setObjectName(QStringLiteral("itemLayout"));
        itemLayout->setContentsMargins(-1, -1, 0, -1);
        statusIconLayout = new QVBoxLayout();
        statusIconLayout->setSpacing(0);
        statusIconLayout->setObjectName(QStringLiteral("statusIconLayout"));
        statusIconLayout->setContentsMargins(2, 2, 2, 2);
        statusLabel = new QLabel(ItemEditWindow);
        statusLabel->setObjectName(QStringLiteral("statusLabel"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(statusLabel->sizePolicy().hasHeightForWidth());
        statusLabel->setSizePolicy(sizePolicy1);
        statusLabel->setMinimumSize(QSize(16, 16));
        statusLabel->setMaximumSize(QSize(32, 32));
        statusLabel->setAlignment(Qt::AlignCenter);

        statusIconLayout->addWidget(statusLabel);


        itemLayout->addLayout(statusIconLayout);

        infoLayout = new QVBoxLayout();
        infoLayout->setSpacing(0);
        infoLayout->setObjectName(QStringLiteral("infoLayout"));
        infoLabel = new QLabel(ItemEditWindow);
        infoLabel->setObjectName(QStringLiteral("infoLabel"));
        QSizePolicy sizePolicy2(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(infoLabel->sizePolicy().hasHeightForWidth());
        infoLabel->setSizePolicy(sizePolicy2);
        infoLabel->setMaximumSize(QSize(16777215, 32));
        infoLabel->setAlignment(Qt::AlignCenter);

        infoLayout->addWidget(infoLabel);


        itemLayout->addLayout(infoLayout);

        deleteButtonLayout = new QVBoxLayout();
        deleteButtonLayout->setSpacing(5);
        deleteButtonLayout->setObjectName(QStringLiteral("deleteButtonLayout"));
        deleteButtonLayout->setContentsMargins(5, 5, 10, 5);
        deleteButton = new QPushButton(ItemEditWindow);
        deleteButton->setObjectName(QStringLiteral("deleteButton"));
        sizePolicy1.setHeightForWidth(deleteButton->sizePolicy().hasHeightForWidth());
        deleteButton->setSizePolicy(sizePolicy1);
        deleteButton->setMinimumSize(QSize(16, 16));
        deleteButton->setMaximumSize(QSize(20, 20));

        deleteButtonLayout->addWidget(deleteButton);


        itemLayout->addLayout(deleteButtonLayout);


        verticalLayout->addLayout(itemLayout);


        retranslateUi(ItemEditWindow);

        QMetaObject::connectSlotsByName(ItemEditWindow);
    } // setupUi

    void retranslateUi(QFrame *ItemEditWindow)
    {
        ItemEditWindow->setWindowTitle(QApplication::translate("ItemEditWindow", "Frame", 0));
        statusLabel->setText(QApplication::translate("ItemEditWindow", "Status", 0));
        infoLabel->setText(QApplication::translate("ItemEditWindow", "Info", 0));
        deleteButton->setText(QApplication::translate("ItemEditWindow", "Delete", 0));
    } // retranslateUi

};

namespace Ui {
    class ItemEditWindow: public Ui_ItemEditWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ITEMEDITWINDOW_H
