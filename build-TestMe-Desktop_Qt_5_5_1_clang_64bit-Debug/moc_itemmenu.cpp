/****************************************************************************
** Meta object code from reading C++ file 'itemmenu.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../TestMe/Menu/ItemMenu/itemmenu.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'itemmenu.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ItemMenu_t {
    QByteArrayData data[5];
    char stringdata0[64];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ItemMenu_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ItemMenu_t qt_meta_stringdata_ItemMenu = {
    {
QT_MOC_LITERAL(0, 0, 8), // "ItemMenu"
QT_MOC_LITERAL(1, 9, 19), // "itemMenuMouseSignal"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 14), // "actionItemMenu"
QT_MOC_LITERAL(4, 45, 18) // "itemMenuMouseClick"

    },
    "ItemMenu\0itemMenuMouseSignal\0\0"
    "actionItemMenu\0itemMenuMouseClick"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ItemMenu[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   24,    2, 0x06 /* Public */,
       4,    1,   27,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    3,

       0        // eod
};

void ItemMenu::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ItemMenu *_t = static_cast<ItemMenu *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->itemMenuMouseSignal((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->itemMenuMouseClick((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (ItemMenu::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ItemMenu::itemMenuMouseSignal)) {
                *result = 0;
            }
        }
        {
            typedef void (ItemMenu::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ItemMenu::itemMenuMouseClick)) {
                *result = 1;
            }
        }
    }
}

const QMetaObject ItemMenu::staticMetaObject = {
    { &QFrame::staticMetaObject, qt_meta_stringdata_ItemMenu.data,
      qt_meta_data_ItemMenu,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ItemMenu::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ItemMenu::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ItemMenu.stringdata0))
        return static_cast<void*>(const_cast< ItemMenu*>(this));
    return QFrame::qt_metacast(_clname);
}

int ItemMenu::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QFrame::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void ItemMenu::itemMenuMouseSignal(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ItemMenu::itemMenuMouseClick(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
