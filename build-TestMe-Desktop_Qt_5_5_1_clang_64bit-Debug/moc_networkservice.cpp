/****************************************************************************
** Meta object code from reading C++ file 'networkservice.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../TestMe/Service/NetworkService/networkservice.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'networkservice.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_NetworkService_t {
    QByteArrayData data[11];
    char stringdata0[137];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_NetworkService_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_NetworkService_t qt_meta_stringdata_NetworkService = {
    {
QT_MOC_LITERAL(0, 0, 14), // "NetworkService"
QT_MOC_LITERAL(1, 15, 14), // "newClientReady"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 11), // "UserObject*"
QT_MOC_LITERAL(4, 43, 9), // "newClient"
QT_MOC_LITERAL(5, 53, 12), // "userPassword"
QT_MOC_LITERAL(6, 66, 16), // "newClientMessage"
QT_MOC_LITERAL(7, 83, 7), // "message"
QT_MOC_LITERAL(8, 91, 16), // "clientDisconnect"
QT_MOC_LITERAL(9, 108, 10), // "userClient"
QT_MOC_LITERAL(10, 119, 17) // "slotNewConnection"

    },
    "NetworkService\0newClientReady\0\0"
    "UserObject*\0newClient\0userPassword\0"
    "newClientMessage\0message\0clientDisconnect\0"
    "userClient\0slotNewConnection"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_NetworkService[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   34,    2, 0x06 /* Public */,
       6,    3,   39,    2, 0x06 /* Public */,
       8,    2,   46,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      10,    0,   51,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3, QMetaType::QString,    4,    5,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 3, QMetaType::QString,    7,    4,    5,
    QMetaType::Void, 0x80000000 | 3, QMetaType::QString,    9,    5,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void NetworkService::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        NetworkService *_t = static_cast<NetworkService *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->newClientReady((*reinterpret_cast< UserObject*(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 1: _t->newClientMessage((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< UserObject*(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3]))); break;
        case 2: _t->clientDisconnect((*reinterpret_cast< UserObject*(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 3: _t->slotNewConnection(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (NetworkService::*_t)(UserObject * , QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&NetworkService::newClientReady)) {
                *result = 0;
            }
        }
        {
            typedef void (NetworkService::*_t)(QString , UserObject * , QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&NetworkService::newClientMessage)) {
                *result = 1;
            }
        }
        {
            typedef void (NetworkService::*_t)(UserObject * , QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&NetworkService::clientDisconnect)) {
                *result = 2;
            }
        }
    }
}

const QMetaObject NetworkService::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_NetworkService.data,
      qt_meta_data_NetworkService,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *NetworkService::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *NetworkService::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_NetworkService.stringdata0))
        return static_cast<void*>(const_cast< NetworkService*>(this));
    return QObject::qt_metacast(_clname);
}

int NetworkService::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void NetworkService::newClientReady(UserObject * _t1, QString _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void NetworkService::newClientMessage(QString _t1, UserObject * _t2, QString _t3)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void NetworkService::clientDisconnect(UserObject * _t1, QString _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
