/****************************************************************************
** Meta object code from reading C++ file 'editgroupwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../TestMe/Window/Group/EditGroup/editgroupwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'editgroupwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_EditGroupWindow_t {
    QByteArrayData data[15];
    char stringdata0[223];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_EditGroupWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_EditGroupWindow_t qt_meta_stringdata_EditGroupWindow = {
    {
QT_MOC_LITERAL(0, 0, 15), // "EditGroupWindow"
QT_MOC_LITERAL(1, 16, 12), // "clientStatus"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 11), // "UserObject*"
QT_MOC_LITERAL(4, 42, 6), // "client"
QT_MOC_LITERAL(5, 49, 10), // "flagStatus"
QT_MOC_LITERAL(6, 60, 12), // "userPassword"
QT_MOC_LITERAL(7, 73, 9), // "newClient"
QT_MOC_LITERAL(8, 83, 10), // "newMessage"
QT_MOC_LITERAL(9, 94, 7), // "message"
QT_MOC_LITERAL(10, 102, 16), // "clientDisconnect"
QT_MOC_LITERAL(11, 119, 17), // "clientLinkDeleted"
QT_MOC_LITERAL(12, 137, 14), // "deleteClientID"
QT_MOC_LITERAL(13, 152, 31), // "on_reconnectpPushButton_clicked"
QT_MOC_LITERAL(14, 184, 38) // "on_editGroupArgumentPushButto..."

    },
    "EditGroupWindow\0clientStatus\0\0UserObject*\0"
    "client\0flagStatus\0userPassword\0newClient\0"
    "newMessage\0message\0clientDisconnect\0"
    "clientLinkDeleted\0deleteClientID\0"
    "on_reconnectpPushButton_clicked\0"
    "on_editGroupArgumentPushButton_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_EditGroupWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    3,   59,    2, 0x06 /* Public */,
       1,    2,   66,    2, 0x26 /* Public | MethodCloned */,
       1,    1,   71,    2, 0x26 /* Public | MethodCloned */,

 // slots: name, argc, parameters, tag, flags
       7,    2,   74,    2, 0x0a /* Public */,
       8,    3,   79,    2, 0x0a /* Public */,
      10,    2,   86,    2, 0x0a /* Public */,
      11,    1,   91,    2, 0x0a /* Public */,
      13,    0,   94,    2, 0x08 /* Private */,
      14,    0,   95,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3, QMetaType::Bool, QMetaType::QString,    4,    5,    6,
    QMetaType::Void, 0x80000000 | 3, QMetaType::Bool,    4,    5,
    QMetaType::Void, 0x80000000 | 3,    4,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3, QMetaType::QString,    4,    6,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 3, QMetaType::QString,    9,    4,    6,
    QMetaType::Void, 0x80000000 | 3, QMetaType::QString,    4,    6,
    QMetaType::Void, QMetaType::QString,   12,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void EditGroupWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        EditGroupWindow *_t = static_cast<EditGroupWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->clientStatus((*reinterpret_cast< UserObject*(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3]))); break;
        case 1: _t->clientStatus((*reinterpret_cast< UserObject*(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 2: _t->clientStatus((*reinterpret_cast< UserObject*(*)>(_a[1]))); break;
        case 3: _t->newClient((*reinterpret_cast< UserObject*(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 4: _t->newMessage((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< UserObject*(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3]))); break;
        case 5: _t->clientDisconnect((*reinterpret_cast< UserObject*(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 6: _t->clientLinkDeleted((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 7: _t->on_reconnectpPushButton_clicked(); break;
        case 8: _t->on_editGroupArgumentPushButton_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (EditGroupWindow::*_t)(UserObject * , bool , QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&EditGroupWindow::clientStatus)) {
                *result = 0;
            }
        }
    }
}

const QMetaObject EditGroupWindow::staticMetaObject = {
    { &QFrame::staticMetaObject, qt_meta_stringdata_EditGroupWindow.data,
      qt_meta_data_EditGroupWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *EditGroupWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *EditGroupWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_EditGroupWindow.stringdata0))
        return static_cast<void*>(const_cast< EditGroupWindow*>(this));
    return QFrame::qt_metacast(_clname);
}

int EditGroupWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QFrame::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void EditGroupWindow::clientStatus(UserObject * _t1, bool _t2, QString _t3)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
