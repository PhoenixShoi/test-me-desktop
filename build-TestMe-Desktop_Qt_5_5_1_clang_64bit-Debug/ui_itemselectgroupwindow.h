/********************************************************************************
** Form generated from reading UI file 'itemselectgroupwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ITEMSELECTGROUPWINDOW_H
#define UI_ITEMSELECTGROUPWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ItemSelectGroupWindow
{
public:
    QHBoxLayout *itemSelectGroupLayout;
    QHBoxLayout *itemLayout;
    QVBoxLayout *informationGroupLayout;
    QVBoxLayout *nameGroupLayout;
    QLabel *nameGroupLabel;
    QHBoxLayout *dateAndPeopleLayout;
    QVBoxLayout *dateLayout;
    QLabel *dateLabel;
    QHBoxLayout *peopleInfoLayout;
    QLabel *peopleLabel;
    QLabel *infoPeopleLabel;
    QVBoxLayout *deleteButtonLayout;
    QPushButton *pushButton;
    QPushButton *deletePushButton;

    void setupUi(QFrame *ItemSelectGroupWindow)
    {
        if (ItemSelectGroupWindow->objectName().isEmpty())
            ItemSelectGroupWindow->setObjectName(QStringLiteral("ItemSelectGroupWindow"));
        ItemSelectGroupWindow->resize(280, 64);
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(ItemSelectGroupWindow->sizePolicy().hasHeightForWidth());
        ItemSelectGroupWindow->setSizePolicy(sizePolicy);
        ItemSelectGroupWindow->setMinimumSize(QSize(0, 64));
        ItemSelectGroupWindow->setMaximumSize(QSize(16777215, 16777215));
        ItemSelectGroupWindow->setAutoFillBackground(true);
        ItemSelectGroupWindow->setFrameShape(QFrame::StyledPanel);
        ItemSelectGroupWindow->setFrameShadow(QFrame::Raised);
        itemSelectGroupLayout = new QHBoxLayout(ItemSelectGroupWindow);
        itemSelectGroupLayout->setSpacing(5);
        itemSelectGroupLayout->setObjectName(QStringLiteral("itemSelectGroupLayout"));
        itemSelectGroupLayout->setSizeConstraint(QLayout::SetMinimumSize);
        itemSelectGroupLayout->setContentsMargins(5, 3, 5, 3);
        itemLayout = new QHBoxLayout();
        itemLayout->setSpacing(0);
        itemLayout->setObjectName(QStringLiteral("itemLayout"));
        itemLayout->setContentsMargins(-1, -1, 0, -1);
        informationGroupLayout = new QVBoxLayout();
        informationGroupLayout->setSpacing(0);
        informationGroupLayout->setObjectName(QStringLiteral("informationGroupLayout"));
        informationGroupLayout->setContentsMargins(-1, -1, 0, 0);
        nameGroupLayout = new QVBoxLayout();
        nameGroupLayout->setSpacing(0);
        nameGroupLayout->setObjectName(QStringLiteral("nameGroupLayout"));
        nameGroupLabel = new QLabel(ItemSelectGroupWindow);
        nameGroupLabel->setObjectName(QStringLiteral("nameGroupLabel"));
        sizePolicy.setHeightForWidth(nameGroupLabel->sizePolicy().hasHeightForWidth());
        nameGroupLabel->setSizePolicy(sizePolicy);
        nameGroupLabel->setMinimumSize(QSize(0, 32));
        nameGroupLabel->setMaximumSize(QSize(16777215, 32));
        nameGroupLabel->setAlignment(Qt::AlignCenter);
        nameGroupLabel->setMargin(0);

        nameGroupLayout->addWidget(nameGroupLabel);


        informationGroupLayout->addLayout(nameGroupLayout);

        dateAndPeopleLayout = new QHBoxLayout();
        dateAndPeopleLayout->setSpacing(0);
        dateAndPeopleLayout->setObjectName(QStringLiteral("dateAndPeopleLayout"));
        dateAndPeopleLayout->setContentsMargins(-1, -1, 0, -1);
        dateLayout = new QVBoxLayout();
        dateLayout->setSpacing(0);
        dateLayout->setObjectName(QStringLiteral("dateLayout"));
        dateLabel = new QLabel(ItemSelectGroupWindow);
        dateLabel->setObjectName(QStringLiteral("dateLabel"));
        sizePolicy.setHeightForWidth(dateLabel->sizePolicy().hasHeightForWidth());
        dateLabel->setSizePolicy(sizePolicy);
        dateLabel->setMinimumSize(QSize(100, 32));
        dateLabel->setMaximumSize(QSize(100, 32));

        dateLayout->addWidget(dateLabel);


        dateAndPeopleLayout->addLayout(dateLayout);

        peopleInfoLayout = new QHBoxLayout();
        peopleInfoLayout->setSpacing(0);
        peopleInfoLayout->setObjectName(QStringLiteral("peopleInfoLayout"));
        peopleLabel = new QLabel(ItemSelectGroupWindow);
        peopleLabel->setObjectName(QStringLiteral("peopleLabel"));
        sizePolicy.setHeightForWidth(peopleLabel->sizePolicy().hasHeightForWidth());
        peopleLabel->setSizePolicy(sizePolicy);
        peopleLabel->setMinimumSize(QSize(64, 32));
        peopleLabel->setMaximumSize(QSize(64, 32));
        peopleLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        peopleInfoLayout->addWidget(peopleLabel);

        infoPeopleLabel = new QLabel(ItemSelectGroupWindow);
        infoPeopleLabel->setObjectName(QStringLiteral("infoPeopleLabel"));
        sizePolicy.setHeightForWidth(infoPeopleLabel->sizePolicy().hasHeightForWidth());
        infoPeopleLabel->setSizePolicy(sizePolicy);
        infoPeopleLabel->setMinimumSize(QSize(64, 32));
        infoPeopleLabel->setMaximumSize(QSize(64, 32));
        infoPeopleLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        peopleInfoLayout->addWidget(infoPeopleLabel);


        dateAndPeopleLayout->addLayout(peopleInfoLayout);


        informationGroupLayout->addLayout(dateAndPeopleLayout);


        itemLayout->addLayout(informationGroupLayout);

        deleteButtonLayout = new QVBoxLayout();
        deleteButtonLayout->setSpacing(0);
        deleteButtonLayout->setObjectName(QStringLiteral("deleteButtonLayout"));
        deleteButtonLayout->setContentsMargins(-1, -1, 0, -1);
        pushButton = new QPushButton(ItemSelectGroupWindow);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        QIcon icon;
        icon.addFile(QStringLiteral(":/icon/generalIcon"), QSize(), QIcon::Normal, QIcon::On);
        pushButton->setIcon(icon);

        deleteButtonLayout->addWidget(pushButton);

        deletePushButton = new QPushButton(ItemSelectGroupWindow);
        deletePushButton->setObjectName(QStringLiteral("deletePushButton"));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/icon/deleteIcon"), QSize(), QIcon::Normal, QIcon::On);
        deletePushButton->setIcon(icon1);

        deleteButtonLayout->addWidget(deletePushButton);


        itemLayout->addLayout(deleteButtonLayout);


        itemSelectGroupLayout->addLayout(itemLayout);


        retranslateUi(ItemSelectGroupWindow);

        QMetaObject::connectSlotsByName(ItemSelectGroupWindow);
    } // setupUi

    void retranslateUi(QFrame *ItemSelectGroupWindow)
    {
        ItemSelectGroupWindow->setWindowTitle(QApplication::translate("ItemSelectGroupWindow", "Frame", 0));
        nameGroupLabel->setText(QApplication::translate("ItemSelectGroupWindow", "Name", 0));
        dateLabel->setText(QApplication::translate("ItemSelectGroupWindow", "Date", 0));
        peopleLabel->setText(QApplication::translate("ItemSelectGroupWindow", "PeopleIcon", 0));
        infoPeopleLabel->setText(QApplication::translate("ItemSelectGroupWindow", "Info", 0));
        pushButton->setText(QString());
        deletePushButton->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class ItemSelectGroupWindow: public Ui_ItemSelectGroupWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ITEMSELECTGROUPWINDOW_H
