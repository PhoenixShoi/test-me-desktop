/********************************************************************************
** Form generated from reading UI file 'editnamegroup.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDITNAMEGROUP_H
#define UI_EDITNAMEGROUP_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_EditNameGroup
{
public:
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *editNameGroupLayout;
    QVBoxLayout *editNameVerticalLayout;
    QLineEdit *nameLineEdit;
    QVBoxLayout *selectButtonVerticalLayout;
    QPushButton *selectPushButton;

    void setupUi(QFrame *EditNameGroup)
    {
        if (EditNameGroup->objectName().isEmpty())
            EditNameGroup->setObjectName(QStringLiteral("EditNameGroup"));
        EditNameGroup->resize(366, 72);
        EditNameGroup->setFrameShape(QFrame::StyledPanel);
        EditNameGroup->setFrameShadow(QFrame::Raised);
        verticalLayout_2 = new QVBoxLayout(EditNameGroup);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setSizeConstraint(QLayout::SetFixedSize);
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        editNameGroupLayout = new QVBoxLayout();
        editNameGroupLayout->setSpacing(0);
        editNameGroupLayout->setObjectName(QStringLiteral("editNameGroupLayout"));
        editNameGroupLayout->setSizeConstraint(QLayout::SetFixedSize);
        editNameGroupLayout->setContentsMargins(-1, -1, -1, 0);
        editNameVerticalLayout = new QVBoxLayout();
        editNameVerticalLayout->setObjectName(QStringLiteral("editNameVerticalLayout"));
        editNameVerticalLayout->setSizeConstraint(QLayout::SetFixedSize);
        editNameVerticalLayout->setContentsMargins(6, 5, 6, 5);
        nameLineEdit = new QLineEdit(EditNameGroup);
        nameLineEdit->setObjectName(QStringLiteral("nameLineEdit"));
        nameLineEdit->setMinimumSize(QSize(350, 0));
        nameLineEdit->setMaximumSize(QSize(350, 16777215));

        editNameVerticalLayout->addWidget(nameLineEdit);


        editNameGroupLayout->addLayout(editNameVerticalLayout);

        selectButtonVerticalLayout = new QVBoxLayout();
        selectButtonVerticalLayout->setObjectName(QStringLiteral("selectButtonVerticalLayout"));
        selectButtonVerticalLayout->setSizeConstraint(QLayout::SetFixedSize);
        selectPushButton = new QPushButton(EditNameGroup);
        selectPushButton->setObjectName(QStringLiteral("selectPushButton"));

        selectButtonVerticalLayout->addWidget(selectPushButton);


        editNameGroupLayout->addLayout(selectButtonVerticalLayout);


        verticalLayout_2->addLayout(editNameGroupLayout);


        retranslateUi(EditNameGroup);

        QMetaObject::connectSlotsByName(EditNameGroup);
    } // setupUi

    void retranslateUi(QFrame *EditNameGroup)
    {
        EditNameGroup->setWindowTitle(QApplication::translate("EditNameGroup", "New name group", 0));
        selectPushButton->setText(QApplication::translate("EditNameGroup", "Select", 0));
    } // retranslateUi

};

namespace Ui {
    class EditNameGroup: public Ui_EditNameGroup {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDITNAMEGROUP_H
