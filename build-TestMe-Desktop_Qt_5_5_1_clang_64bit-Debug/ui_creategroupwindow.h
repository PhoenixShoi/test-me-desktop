/********************************************************************************
** Form generated from reading UI file 'creategroupwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CREATEGROUPWINDOW_H
#define UI_CREATEGROUPWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_CreateGroupWindow
{
public:
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *inputLayout;
    QVBoxLayout *inputDataLayout;
    QVBoxLayout *inputNameLayout;
    QVBoxLayout *inputNameLabelLayout;
    QLabel *inputNameLabel;
    QVBoxLayout *inputNameLineEditLayout;
    QLineEdit *inputNameLineEdit;
    QVBoxLayout *createGroupButtonLayout;
    QPushButton *createGroupButton;

    void setupUi(QFrame *CreateGroupWindow)
    {
        if (CreateGroupWindow->objectName().isEmpty())
            CreateGroupWindow->setObjectName(QStringLiteral("CreateGroupWindow"));
        CreateGroupWindow->resize(291, 89);
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(CreateGroupWindow->sizePolicy().hasHeightForWidth());
        CreateGroupWindow->setSizePolicy(sizePolicy);
        CreateGroupWindow->setFrameShape(QFrame::StyledPanel);
        CreateGroupWindow->setFrameShadow(QFrame::Raised);
        verticalLayout_2 = new QVBoxLayout(CreateGroupWindow);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        inputLayout = new QVBoxLayout();
        inputLayout->setSpacing(0);
        inputLayout->setObjectName(QStringLiteral("inputLayout"));
        inputLayout->setContentsMargins(-1, -1, -1, 0);
        inputDataLayout = new QVBoxLayout();
        inputDataLayout->setObjectName(QStringLiteral("inputDataLayout"));
        inputNameLayout = new QVBoxLayout();
        inputNameLayout->setSpacing(0);
        inputNameLayout->setObjectName(QStringLiteral("inputNameLayout"));
        inputNameLayout->setContentsMargins(-1, 0, 0, -1);
        inputNameLabelLayout = new QVBoxLayout();
        inputNameLabelLayout->setSpacing(5);
        inputNameLabelLayout->setObjectName(QStringLiteral("inputNameLabelLayout"));
        inputNameLabelLayout->setContentsMargins(5, 3, 5, 3);
        inputNameLabel = new QLabel(CreateGroupWindow);
        inputNameLabel->setObjectName(QStringLiteral("inputNameLabel"));
        sizePolicy.setHeightForWidth(inputNameLabel->sizePolicy().hasHeightForWidth());
        inputNameLabel->setSizePolicy(sizePolicy);

        inputNameLabelLayout->addWidget(inputNameLabel);


        inputNameLayout->addLayout(inputNameLabelLayout);

        inputNameLineEditLayout = new QVBoxLayout();
        inputNameLineEditLayout->setSpacing(0);
        inputNameLineEditLayout->setObjectName(QStringLiteral("inputNameLineEditLayout"));
        inputNameLineEditLayout->setContentsMargins(5, 3, 5, 3);
        inputNameLineEdit = new QLineEdit(CreateGroupWindow);
        inputNameLineEdit->setObjectName(QStringLiteral("inputNameLineEdit"));
        QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(inputNameLineEdit->sizePolicy().hasHeightForWidth());
        inputNameLineEdit->setSizePolicy(sizePolicy1);

        inputNameLineEditLayout->addWidget(inputNameLineEdit);


        inputNameLayout->addLayout(inputNameLineEditLayout);


        inputDataLayout->addLayout(inputNameLayout);


        inputLayout->addLayout(inputDataLayout);

        createGroupButtonLayout = new QVBoxLayout();
        createGroupButtonLayout->setSpacing(0);
        createGroupButtonLayout->setObjectName(QStringLiteral("createGroupButtonLayout"));
        createGroupButton = new QPushButton(CreateGroupWindow);
        createGroupButton->setObjectName(QStringLiteral("createGroupButton"));

        createGroupButtonLayout->addWidget(createGroupButton);


        inputLayout->addLayout(createGroupButtonLayout);


        verticalLayout_2->addLayout(inputLayout);


        retranslateUi(CreateGroupWindow);

        QMetaObject::connectSlotsByName(CreateGroupWindow);
    } // setupUi

    void retranslateUi(QFrame *CreateGroupWindow)
    {
        CreateGroupWindow->setWindowTitle(QApplication::translate("CreateGroupWindow", "Create new group", 0));
        inputNameLabel->setText(QApplication::translate("CreateGroupWindow", "Name group", 0));
        inputNameLineEdit->setInputMask(QString());
        inputNameLineEdit->setPlaceholderText(QApplication::translate("CreateGroupWindow", "Enter group name", 0));
        createGroupButton->setText(QApplication::translate("CreateGroupWindow", "Create", 0));
    } // retranslateUi

};

namespace Ui {
    class CreateGroupWindow: public Ui_CreateGroupWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CREATEGROUPWINDOW_H
