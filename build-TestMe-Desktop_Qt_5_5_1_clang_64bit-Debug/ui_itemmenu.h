/********************************************************************************
** Form generated from reading UI file 'itemmenu.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ITEMMENU_H
#define UI_ITEMMENU_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ItemMenu
{
public:
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *iconLayout;
    QLabel *iconLabel;
    QVBoxLayout *infoLayout;
    QLabel *infoLabel;

    void setupUi(QFrame *ItemMenu)
    {
        if (ItemMenu->objectName().isEmpty())
            ItemMenu->setObjectName(QStringLiteral("ItemMenu"));
        ItemMenu->resize(149, 74);
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(ItemMenu->sizePolicy().hasHeightForWidth());
        ItemMenu->setSizePolicy(sizePolicy);
        ItemMenu->setMinimumSize(QSize(128, 64));
        ItemMenu->setAutoFillBackground(true);
        ItemMenu->setStyleSheet(QStringLiteral("green"));
        ItemMenu->setFrameShape(QFrame::StyledPanel);
        ItemMenu->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(ItemMenu);
        horizontalLayout->setSpacing(5);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetMinimumSize);
        horizontalLayout->setContentsMargins(5, 3, 5, 3);
        iconLayout = new QVBoxLayout();
        iconLayout->setSpacing(0);
        iconLayout->setObjectName(QStringLiteral("iconLayout"));
        iconLayout->setSizeConstraint(QLayout::SetMinimumSize);
        iconLabel = new QLabel(ItemMenu);
        iconLabel->setObjectName(QStringLiteral("iconLabel"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(iconLabel->sizePolicy().hasHeightForWidth());
        iconLabel->setSizePolicy(sizePolicy1);
        iconLabel->setMinimumSize(QSize(64, 64));
        iconLabel->setMaximumSize(QSize(64, 64));
        iconLabel->setAlignment(Qt::AlignCenter);
        iconLabel->setMargin(0);

        iconLayout->addWidget(iconLabel);


        horizontalLayout->addLayout(iconLayout);

        infoLayout = new QVBoxLayout();
        infoLayout->setSpacing(0);
        infoLayout->setObjectName(QStringLiteral("infoLayout"));
        infoLayout->setSizeConstraint(QLayout::SetMinimumSize);
        infoLabel = new QLabel(ItemMenu);
        infoLabel->setObjectName(QStringLiteral("infoLabel"));
        QSizePolicy sizePolicy2(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(infoLabel->sizePolicy().hasHeightForWidth());
        infoLabel->setSizePolicy(sizePolicy2);
        infoLabel->setMinimumSize(QSize(64, 64));
        infoLabel->setMaximumSize(QSize(16777215, 64));
        infoLabel->setAlignment(Qt::AlignCenter);

        infoLayout->addWidget(infoLabel);


        horizontalLayout->addLayout(infoLayout);


        retranslateUi(ItemMenu);

        QMetaObject::connectSlotsByName(ItemMenu);
    } // setupUi

    void retranslateUi(QFrame *ItemMenu)
    {
        ItemMenu->setWindowTitle(QApplication::translate("ItemMenu", "Frame", 0));
        iconLabel->setText(QApplication::translate("ItemMenu", "Icon", 0));
        infoLabel->setText(QApplication::translate("ItemMenu", "Info", 0));
    } // retranslateUi

};

namespace Ui {
    class ItemMenu: public Ui_ItemMenu {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ITEMMENU_H
