/****************************************************************************
** Meta object code from reading C++ file 'itemselectgroupwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../TestMe/Window/Group/SelectGroup/ItemSelectGroupWindow/itemselectgroupwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'itemselectgroupwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ItemSelectGroupWindow_t {
    QByteArrayData data[7];
    char stringdata0[120];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ItemSelectGroupWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ItemSelectGroupWindow_t qt_meta_stringdata_ItemSelectGroupWindow = {
    {
QT_MOC_LITERAL(0, 0, 21), // "ItemSelectGroupWindow"
QT_MOC_LITERAL(1, 22, 18), // "itemMenuMouseClick"
QT_MOC_LITERAL(2, 41, 0), // ""
QT_MOC_LITERAL(3, 42, 20), // "deleteButtonFromItem"
QT_MOC_LITERAL(4, 63, 6), // "itemID"
QT_MOC_LITERAL(5, 70, 27), // "on_deletePushButton_clicked"
QT_MOC_LITERAL(6, 98, 21) // "on_pushButton_clicked"

    },
    "ItemSelectGroupWindow\0itemMenuMouseClick\0"
    "\0deleteButtonFromItem\0itemID\0"
    "on_deletePushButton_clicked\0"
    "on_pushButton_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ItemSelectGroupWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x06 /* Public */,
       3,    1,   35,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    0,   38,    2, 0x08 /* Private */,
       6,    0,   39,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    4,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void ItemSelectGroupWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ItemSelectGroupWindow *_t = static_cast<ItemSelectGroupWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->itemMenuMouseClick(); break;
        case 1: _t->deleteButtonFromItem((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->on_deletePushButton_clicked(); break;
        case 3: _t->on_pushButton_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (ItemSelectGroupWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ItemSelectGroupWindow::itemMenuMouseClick)) {
                *result = 0;
            }
        }
        {
            typedef void (ItemSelectGroupWindow::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ItemSelectGroupWindow::deleteButtonFromItem)) {
                *result = 1;
            }
        }
    }
}

const QMetaObject ItemSelectGroupWindow::staticMetaObject = {
    { &QFrame::staticMetaObject, qt_meta_stringdata_ItemSelectGroupWindow.data,
      qt_meta_data_ItemSelectGroupWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ItemSelectGroupWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ItemSelectGroupWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ItemSelectGroupWindow.stringdata0))
        return static_cast<void*>(const_cast< ItemSelectGroupWindow*>(this));
    return QFrame::qt_metacast(_clname);
}

int ItemSelectGroupWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QFrame::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void ItemSelectGroupWindow::itemMenuMouseClick()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void ItemSelectGroupWindow::deleteButtonFromItem(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
