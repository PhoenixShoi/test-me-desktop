/********************************************************************************
** Form generated from reading UI file 'editgroupwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDITGROUPWINDOW_H
#define UI_EDITGROUPWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_EditGroupWindow
{
public:
    QVBoxLayout *verticalLayout_8;
    QHBoxLayout *editWindowLayout;
    QHBoxLayout *listClientLayout;
    QGroupBox *listClientGroupBox;
    QHBoxLayout *horizontalLayout_3;
    QScrollArea *listClientScrollArea;
    QWidget *listClientScrollAreaWidgetContents;
    QVBoxLayout *verticalLayout_5;
    QVBoxLayout *listLayout;
    QSpacerItem *verticalSpacer;
    QVBoxLayout *infoLayout;
    QVBoxLayout *infoConnectionLayout;
    QLabel *infoConnectionLabel;
    QVBoxLayout *infoGraphicAndLogAndIconLayout;
    QHBoxLayout *iconPanelLayout;
    QVBoxLayout *countRegisterUserLayout;
    QPushButton *infoCountRegisterUserPushButton;
    QVBoxLayout *countConnectionUserLayout;
    QPushButton *infoCountConnectionUserPushButton;
    QVBoxLayout *editGroupArgumentLayout;
    QPushButton *editGroupArgumentPushButton;
    QVBoxLayout *chatLayout;
    QPlainTextEdit *chatPlainTextEdit;
    QVBoxLayout *logLayout;
    QLineEdit *logLineEdit;
    QPushButton *reconnectpPushButton;

    void setupUi(QFrame *EditGroupWindow)
    {
        if (EditGroupWindow->objectName().isEmpty())
            EditGroupWindow->setObjectName(QStringLiteral("EditGroupWindow"));
        EditGroupWindow->resize(573, 316);
        EditGroupWindow->setFrameShape(QFrame::StyledPanel);
        EditGroupWindow->setFrameShadow(QFrame::Raised);
        verticalLayout_8 = new QVBoxLayout(EditGroupWindow);
        verticalLayout_8->setSpacing(0);
        verticalLayout_8->setObjectName(QStringLiteral("verticalLayout_8"));
        verticalLayout_8->setContentsMargins(0, 0, 0, 0);
        editWindowLayout = new QHBoxLayout();
        editWindowLayout->setSpacing(0);
        editWindowLayout->setObjectName(QStringLiteral("editWindowLayout"));
        editWindowLayout->setContentsMargins(-1, -1, 0, -1);
        listClientLayout = new QHBoxLayout();
        listClientLayout->setSpacing(0);
        listClientLayout->setObjectName(QStringLiteral("listClientLayout"));
        listClientLayout->setSizeConstraint(QLayout::SetMinimumSize);
        listClientGroupBox = new QGroupBox(EditGroupWindow);
        listClientGroupBox->setObjectName(QStringLiteral("listClientGroupBox"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(listClientGroupBox->sizePolicy().hasHeightForWidth());
        listClientGroupBox->setSizePolicy(sizePolicy);
        horizontalLayout_3 = new QHBoxLayout(listClientGroupBox);
        horizontalLayout_3->setSpacing(0);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setSizeConstraint(QLayout::SetMinimumSize);
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        listClientScrollArea = new QScrollArea(listClientGroupBox);
        listClientScrollArea->setObjectName(QStringLiteral("listClientScrollArea"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(listClientScrollArea->sizePolicy().hasHeightForWidth());
        listClientScrollArea->setSizePolicy(sizePolicy1);
        listClientScrollArea->setMinimumSize(QSize(250, 0));
        listClientScrollArea->setWidgetResizable(true);
        listClientScrollAreaWidgetContents = new QWidget();
        listClientScrollAreaWidgetContents->setObjectName(QStringLiteral("listClientScrollAreaWidgetContents"));
        listClientScrollAreaWidgetContents->setGeometry(QRect(0, 0, 248, 283));
        sizePolicy.setHeightForWidth(listClientScrollAreaWidgetContents->sizePolicy().hasHeightForWidth());
        listClientScrollAreaWidgetContents->setSizePolicy(sizePolicy);
        listClientScrollAreaWidgetContents->setMinimumSize(QSize(245, 0));
        verticalLayout_5 = new QVBoxLayout(listClientScrollAreaWidgetContents);
        verticalLayout_5->setSpacing(0);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        listLayout = new QVBoxLayout();
        listLayout->setSpacing(3);
        listLayout->setObjectName(QStringLiteral("listLayout"));
        listLayout->setContentsMargins(3, 2, 3, 2);
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        listLayout->addItem(verticalSpacer);


        verticalLayout_5->addLayout(listLayout);

        listClientScrollArea->setWidget(listClientScrollAreaWidgetContents);

        horizontalLayout_3->addWidget(listClientScrollArea);


        listClientLayout->addWidget(listClientGroupBox);


        editWindowLayout->addLayout(listClientLayout);

        infoLayout = new QVBoxLayout();
        infoLayout->setSpacing(0);
        infoLayout->setObjectName(QStringLiteral("infoLayout"));
        infoConnectionLayout = new QVBoxLayout();
        infoConnectionLayout->setObjectName(QStringLiteral("infoConnectionLayout"));
        infoConnectionLabel = new QLabel(EditGroupWindow);
        infoConnectionLabel->setObjectName(QStringLiteral("infoConnectionLabel"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Minimum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(infoConnectionLabel->sizePolicy().hasHeightForWidth());
        infoConnectionLabel->setSizePolicy(sizePolicy2);
        infoConnectionLabel->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        infoConnectionLayout->addWidget(infoConnectionLabel);


        infoLayout->addLayout(infoConnectionLayout);

        infoGraphicAndLogAndIconLayout = new QVBoxLayout();
        infoGraphicAndLogAndIconLayout->setSpacing(0);
        infoGraphicAndLogAndIconLayout->setObjectName(QStringLiteral("infoGraphicAndLogAndIconLayout"));
        infoGraphicAndLogAndIconLayout->setContentsMargins(-1, -1, -1, 0);
        iconPanelLayout = new QHBoxLayout();
        iconPanelLayout->setSpacing(0);
        iconPanelLayout->setObjectName(QStringLiteral("iconPanelLayout"));
        iconPanelLayout->setContentsMargins(-1, -1, 0, -1);
        countRegisterUserLayout = new QVBoxLayout();
        countRegisterUserLayout->setObjectName(QStringLiteral("countRegisterUserLayout"));
        infoCountRegisterUserPushButton = new QPushButton(EditGroupWindow);
        infoCountRegisterUserPushButton->setObjectName(QStringLiteral("infoCountRegisterUserPushButton"));
        QIcon icon;
        icon.addFile(QStringLiteral(":/icon/peopleIcon"), QSize(), QIcon::Normal, QIcon::On);
        infoCountRegisterUserPushButton->setIcon(icon);
        infoCountRegisterUserPushButton->setCheckable(false);
        infoCountRegisterUserPushButton->setAutoExclusive(true);
        infoCountRegisterUserPushButton->setFlat(true);

        countRegisterUserLayout->addWidget(infoCountRegisterUserPushButton);


        iconPanelLayout->addLayout(countRegisterUserLayout);

        countConnectionUserLayout = new QVBoxLayout();
        countConnectionUserLayout->setObjectName(QStringLiteral("countConnectionUserLayout"));
        infoCountConnectionUserPushButton = new QPushButton(EditGroupWindow);
        infoCountConnectionUserPushButton->setObjectName(QStringLiteral("infoCountConnectionUserPushButton"));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/icon/connectIcon"), QSize(), QIcon::Normal, QIcon::On);
        infoCountConnectionUserPushButton->setIcon(icon1);
        infoCountConnectionUserPushButton->setCheckable(false);
        infoCountConnectionUserPushButton->setAutoRepeatDelay(10);
        infoCountConnectionUserPushButton->setAutoRepeatInterval(10);
        infoCountConnectionUserPushButton->setFlat(true);

        countConnectionUserLayout->addWidget(infoCountConnectionUserPushButton);


        iconPanelLayout->addLayout(countConnectionUserLayout);

        editGroupArgumentLayout = new QVBoxLayout();
        editGroupArgumentLayout->setObjectName(QStringLiteral("editGroupArgumentLayout"));
        editGroupArgumentPushButton = new QPushButton(EditGroupWindow);
        editGroupArgumentPushButton->setObjectName(QStringLiteral("editGroupArgumentPushButton"));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/icon/generalIcon"), QSize(), QIcon::Normal, QIcon::On);
        editGroupArgumentPushButton->setIcon(icon2);
        editGroupArgumentPushButton->setCheckable(false);
        editGroupArgumentPushButton->setChecked(false);
        editGroupArgumentPushButton->setFlat(true);

        editGroupArgumentLayout->addWidget(editGroupArgumentPushButton);


        iconPanelLayout->addLayout(editGroupArgumentLayout);


        infoGraphicAndLogAndIconLayout->addLayout(iconPanelLayout);

        chatLayout = new QVBoxLayout();
        chatLayout->setObjectName(QStringLiteral("chatLayout"));
        chatPlainTextEdit = new QPlainTextEdit(EditGroupWindow);
        chatPlainTextEdit->setObjectName(QStringLiteral("chatPlainTextEdit"));
        chatPlainTextEdit->setAutoFillBackground(false);
        chatPlainTextEdit->setUndoRedoEnabled(false);
        chatPlainTextEdit->setReadOnly(true);
        chatPlainTextEdit->setTextInteractionFlags(Qt::NoTextInteraction);
        chatPlainTextEdit->setBackgroundVisible(false);

        chatLayout->addWidget(chatPlainTextEdit);


        infoGraphicAndLogAndIconLayout->addLayout(chatLayout);

        logLayout = new QVBoxLayout();
        logLayout->setSpacing(0);
        logLayout->setObjectName(QStringLiteral("logLayout"));
        logLayout->setContentsMargins(5, 6, 5, 6);
        logLineEdit = new QLineEdit(EditGroupWindow);
        logLineEdit->setObjectName(QStringLiteral("logLineEdit"));
        logLineEdit->setFrame(false);
        logLineEdit->setAlignment(Qt::AlignCenter);
        logLineEdit->setReadOnly(true);
        logLineEdit->setClearButtonEnabled(false);

        logLayout->addWidget(logLineEdit);

        reconnectpPushButton = new QPushButton(EditGroupWindow);
        reconnectpPushButton->setObjectName(QStringLiteral("reconnectpPushButton"));
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/icon/restartConnectionIcon"), QSize(), QIcon::Normal, QIcon::On);
        reconnectpPushButton->setIcon(icon3);

        logLayout->addWidget(reconnectpPushButton);


        infoGraphicAndLogAndIconLayout->addLayout(logLayout);


        infoLayout->addLayout(infoGraphicAndLogAndIconLayout);


        editWindowLayout->addLayout(infoLayout);


        verticalLayout_8->addLayout(editWindowLayout);


        retranslateUi(EditGroupWindow);

        QMetaObject::connectSlotsByName(EditGroupWindow);
    } // setupUi

    void retranslateUi(QFrame *EditGroupWindow)
    {
        EditGroupWindow->setWindowTitle(QApplication::translate("EditGroupWindow", "Edit Group", 0));
        listClientGroupBox->setTitle(QApplication::translate("EditGroupWindow", "List Client", 0));
        infoConnectionLabel->setText(QApplication::translate("EditGroupWindow", "Information connection", 0));
        infoCountRegisterUserPushButton->setText(QApplication::translate("EditGroupWindow", "0", 0));
        infoCountConnectionUserPushButton->setText(QApplication::translate("EditGroupWindow", "0", 0));
        editGroupArgumentPushButton->setText(QString());
        reconnectpPushButton->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class EditGroupWindow: public Ui_EditGroupWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDITGROUPWINDOW_H
