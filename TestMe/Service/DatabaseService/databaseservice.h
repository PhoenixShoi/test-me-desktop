#ifndef DATABASESERVICE_H
#define DATABASESERVICE_H

#include "Object/Group/groupobject.h"
#include "Object/User/userobject.h"

#include <QSql>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <vector>

class DatabaseService
{
public:
    DatabaseService();
    ~DatabaseService();
    void getAllGroup(std::vector<GroupObject> *returnVectorGroups);
    void getAllUsersFromGroup(std::vector<UserObject> *returnVectorUsers,QString groupID);
    void getAllUsers(std::vector<UserObject> *returnVectorUsers);
    void addGroup(GroupObject newGroupInDatabase);
    void deleteGroup(QString targetID);
    void deleteConnectUserAndGroup(QString userID,QString groupID);
    void addUser(UserObject *newUserInDatabase,QString password="");
    bool checkUser(UserObject *searchUser);
    bool checkUserInGroup(QString userID,QString groupID);
    void createConnectUserAndGroup(QString userID,QString groupID);
    void editGroup(QString groupID,QString newGroupName);
private:
    QSqlDatabase db;

    void queryDatabaseNotRequest(QString queryStr);
};

#endif // DATABASESERVICE_H
