#include "databaseservice.h"
#include <QMessageBox>
#include <QDebug>

DatabaseService::DatabaseService()
{
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("testmedatabase.db");
    db.setUserName("pxtestme");
    db.setHostName("infinityteam");
    db.setPassword("pxinfinity");

    if(!db.open())
    {
        QMessageBox::critical(0,"Error","Error Database open");
    }
    else
    {
        if(!db.tables().length())
        {
            this->queryDatabaseNotRequest("CREATE TABLE `groupTable` ( `groupID`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `name`	TEXT NOT NULL, `date`	TEXT NOT NULL);");
            this->queryDatabaseNotRequest("CREATE TABLE `peopleTable` (`peopleID`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `name`	TEXT NOT NULL, `login`	TEXT NOT NULL, `password`	TEXT NOT NULL);");
            this->queryDatabaseNotRequest("CREATE TABLE `peopleIDgroupID` ( `peopleID`	TEXT NOT NULL, `groupID`	TEXT NOT NULL);");
        }
    }

}

DatabaseService::~DatabaseService()
{
}

void DatabaseService::getAllGroup(std::vector<GroupObject> *returnVectorGroups)
{
    QSqlQuery query;
    query.exec("SELECT * FROM groupTable");
    QSqlRecord rec = query.record();
    while(query.next())
    {
        QString preQueryStr="SELECT * FROM peopleIDgroupID WHERE groupID = '%1'";
        QString queryStr=preQueryStr.arg(query.value(rec.indexOf("groupID")).toString());
        QSqlQuery queryCount;
        queryCount.exec(queryStr);

        int count=0;
        while(queryCount.next())
            count++;
        qDebug()<<"Database group count"<<count<<endl;

        GroupObject grObject(
                        query.value(rec.indexOf("groupID")).toString(),
                        query.value(rec.indexOf("name")).toString(),
                        query.value(rec.indexOf("date")).toString(),
                        count
                    );

        returnVectorGroups->push_back(grObject);
    }

}

void DatabaseService::getAllUsersFromGroup(std::vector<UserObject> *returnVectorUsers, QString groupID)
{
    QString preQueryStr="SELECT * FROM peopleIDgroupID WHERE groupID = '%1'";
    QString queryStr=preQueryStr.arg(groupID);
    QSqlQuery query;
    query.exec(queryStr);
    QSqlRecord rec = query.record();
    while(query.next())
    {
        preQueryStr="SELECT * FROM peopleTable WHERE peopleID = '%1'";
        queryStr=preQueryStr.arg(query.value(rec.indexOf("peopleID")).toString());

        QSqlQuery usersQuery;
        usersQuery.exec(queryStr);
        QSqlRecord usrRec = usersQuery.record();

        while(usersQuery.next())
        {
            UserObject usObject(usersQuery.value(usrRec.indexOf("peopleID")).toString(),
                                usersQuery.value(usrRec.indexOf("name")).toString(),
                                usersQuery.value(usrRec.indexOf("login")).toString(),
                                usersQuery.value(usrRec.indexOf("password")).toString()
                        );
            returnVectorUsers->push_back(usObject);
        }
    }
}

void DatabaseService::getAllUsers(std::vector<UserObject> *returnVectorUsers)
{
    QSqlQuery query;
    query.exec("SELECT * FROM peopleTable");
    QSqlRecord rec = query.record();
    while(query.next())
    {
        UserObject usObject(query.value(rec.indexOf("peopleID")).toString(),
                            query.value(rec.indexOf("name")).toString(),
                            query.value(rec.indexOf("login")).toString(),
                            query.value(rec.indexOf("password")).toString()
                    );
        returnVectorUsers->push_back(usObject);
    }
}

void DatabaseService::addGroup(GroupObject newGroupInDatabase)
{
   QString preQueryStr="INSERT INTO groupTable (name,date)"
           "VALUES('%1','%2')";
   QString queryStr=preQueryStr.arg(newGroupInDatabase.getName())
                               .arg(newGroupInDatabase.getDate());
   this->queryDatabaseNotRequest(queryStr);
}

void DatabaseService::deleteGroup(QString targetID)
{
    QString preQueryStr="DELETE FROM groupTable WHERE groupID = '%1'";
    QString queryStr=preQueryStr.arg(targetID);
    this->queryDatabaseNotRequest(queryStr);
    preQueryStr="DELETE FROM peopleIDgroupID WHERE groupID = '%1'";
    queryStr=preQueryStr.arg(targetID);
    this->queryDatabaseNotRequest(queryStr);
}

void DatabaseService::deleteConnectUserAndGroup(QString userID, QString groupID)
{
    QString preQueryStr="DELETE FROM peopleIDgroupID WHERE peopleID = '%1' AND groupID = '%2'";
    QString queryStr=preQueryStr.arg(userID)
                                .arg(groupID);
    this->queryDatabaseNotRequest(queryStr);
}

bool DatabaseService::checkUser(UserObject *searchUser)
{
    QString preQueryStr="SELECT * FROM peopleTable WHERE login = '%1'";
    QString queryStr=preQueryStr.arg(searchUser->getLogin());

    QSqlQuery query;
    query.exec(queryStr);

    QSqlRecord rec = query.record();
    while(query.next())
    {
        if(searchUser->checkPassword(query.value(rec.indexOf("password")).toString()))
        {
            searchUser->setID(query.value(rec.indexOf("peopleID")).toString());
            return true;
        }
    }
    return false;
}

bool DatabaseService::checkUserInGroup(QString userID, QString groupID)
{
    QString preQueryStr="SELECT * FROM peopleIDgroupID WHERE peopleID = '%1' AND groupID = '%2'";
    QString queryStr=preQueryStr.arg(userID)
                                .arg(groupID);

    QSqlQuery query;
    query.exec(queryStr);

    QSqlRecord rec = query.record();

    while(query.next())
    {
        return true;
    }


    return false;
}

void DatabaseService::createConnectUserAndGroup(QString userID, QString groupID)
{
    qDebug()<<"Create database string userId="<<userID<<" and groupID="<<endl;
    QString preQueryStr="INSERT INTO peopleIDgroupID (peopleID,groupID)"
            "VALUES('%1','%2')";
    QString queryStr=preQueryStr.arg(userID)
                                .arg(groupID);
    this->queryDatabaseNotRequest(queryStr);
}

void DatabaseService::editGroup(QString groupID,QString newGroupName)
{
    QString preQueryStr="UPDATE groupTable SET name='%1' WHERE groupID='%2'";
    QString queryStr=preQueryStr.arg(newGroupName)
                                .arg(groupID);
    this->queryDatabaseNotRequest(queryStr);
    ;
}

void DatabaseService::addUser(UserObject *newUserInDatabase,QString passwordNewUser)
{
    QString preQueryStr="INSERT INTO peopleTable (name,login,password)"
            "VALUES('%1','%2','%3')";
    QString queryStr=preQueryStr.arg(newUserInDatabase->getName())
                                .arg(newUserInDatabase->getLogin())
                                .arg(passwordNewUser);
    this->queryDatabaseNotRequest(queryStr);
}

void DatabaseService::queryDatabaseNotRequest(QString queryStr)
{
    QSqlQuery query;
    query.exec(queryStr);
}

