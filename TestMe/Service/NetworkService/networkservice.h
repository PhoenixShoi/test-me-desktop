#ifndef NETWORKSERVICE_H
#define NETWORKSERVICE_H

#include "Object/User/userobject.h"

#include <QTcpServer>
#include <QTcpSocket>
#include <QString>


class NetworkService : public QObject
{
    Q_OBJECT

public:
    NetworkService();
    ~NetworkService();

    QString getServerAddress();
    void startServer();

private:
    QTcpServer *tcpServer;
    quint32 nextBlockSize=0;

signals:
    void newClientReady(UserObject *newClient,QString userPassword);
    void newClientMessage(QString message,UserObject *newClient,QString userPassword);
    void clientDisconnect(UserObject *userClient,QString userPassword);
protected slots:
    void slotNewConnection();
};


#endif // NETWORKSERVICE_H

