#include "networkservice.h"


#include "Service/NetworkService/NetworkClient/networkclient.h"

#include <QNetworkInterface>
#include <QSettings>

NetworkService::NetworkService()
{
    tcpServer = new QTcpServer(this);
    this->startServer();
    connect(tcpServer,SIGNAL(newConnection()),this,SLOT(slotNewConnection()));
}

NetworkService::~NetworkService()
{
}

void NetworkService::slotNewConnection()
{
    NetworkClient *newClient = new NetworkClient(tcpServer);
    connect(newClient,SIGNAL(clientReady(UserObject*,QString)),this,SIGNAL(newClientReady(UserObject*,QString)));
    connect(newClient,SIGNAL(clientSendMessage(QString,UserObject*,QString)),this,SIGNAL(newClientMessage(QString,UserObject*,QString)));
    connect(newClient,SIGNAL(clientDisconnect(UserObject*,QString)),this,SIGNAL(clientDisconnect(UserObject*,QString)));
}

QString NetworkService::getServerAddress()
{
    foreach (const QHostAddress &address, QNetworkInterface::allAddresses()) {
        if (address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost))
            return address.toString();
    }
    return "";
}

void NetworkService::startServer()
{
    QSettings setting;
    if(setting.value("/Network/Port","").toString()=="")
    {
        setting.setValue("/Network/Port",2323);
    }
    qDebug()<<"Real server port"<<setting.value("/Network/Port","Not Value").toString()<<endl;
    if(!tcpServer->listen(QHostAddress::Any,setting.value("/Network/Port",2323).toInt()))
    {
        tcpServer->close();
        return;
    }
}


