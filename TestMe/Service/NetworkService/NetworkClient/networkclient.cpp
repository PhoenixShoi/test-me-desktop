#include "networkclient.h"

#include <QDebug>

NetworkClient::NetworkClient(QTcpServer *tcpServer,QObject *parent) : QObject(parent)
{
    this->userClient= new UserObject();
    this->clientSocket = tcpServer->nextPendingConnection();
    connect(clientSocket,SIGNAL(disconnected()),this,SLOT(slotDisconnectClient()));
    connect(clientSocket,SIGNAL(readyRead()),this,SLOT(slotReadClient()));
    qDebug()<<"New Connect"<<clientSocket->peerAddress()<<endl;
}

void NetworkClient::controlManager(QString command)
{
    if(flagReady)
    {
        if(command.mid(0,2)=="m_")
        {
            emit clientSendMessage(command.mid(2,command.length()-2),userClient,userPassword);
            qDebug()<<command.mid(2,command.length()-2)<<endl;
            return;
        }
        if(command.mid(0,2)=="d_")
        {
            emit clientDisconnect(userClient,userPassword);
            qDebug()<<command.mid(2,command.length()-2)<<endl;
            return;
        }
    }
    else
    {
        if(command.mid(0,2)=="n_")
        {
            this->userClient=new UserObject("-1",
                                            command.mid(2,command.length()-2),
                                            userClient->getLogin(),
                                            userPassword
                                            );
            qDebug()<<command.mid(2,command.length()-2)<<endl;
            this->checkReady();
            return;
        }
        if(command.mid(0,2)=="l_")
        {
            this->userClient=new UserObject("-1",
                                            userClient->getName(),
                                            command.mid(2,command.length()-2),
                                            userPassword
                                            );
            qDebug()<<command.mid(2,command.length()-2)<<endl;
            this->checkReady();
            return;
        }
        if(command.mid(0,2)=="p_")
        {
            userPassword=command.mid(2,command.length()-2);
            this->userClient=new UserObject("-1",
                                            userClient->getName(),
                                            userClient->getLogin(),
                                            userPassword
                                         );
            qDebug()<<command.mid(2,command.length()-2)<<endl;
            this->checkReady();
            return;
        }
    }
}

void NetworkClient::checkReady()
{
    if((userClient->getLogin()!="")&&
       (userClient->getName()!="")&&
       (userPassword!=""))
    {
        flagReady=true;
        emit clientReady(userClient,userPassword);
        qDebug()<<"Client ready"<<endl;
    }
}

void NetworkClient::slotReadClient()
{
    QString message="";
    while(true)
    {
        if(clientSocket->canReadLine())
        {
            message=QString::fromUtf8(clientSocket->readLine().trimmed());
            this->controlManager(message);
            qDebug()<<clientSocket->peerAddress()<<" "<<message<<endl;
            break;
        }
    }
}

void NetworkClient::slotDisconnectClient()
{
    qDebug()<<clientSocket->peerAddress()<<" disconnect"<<endl;
    clientSocket->deleteLater();
    if(flagReady)
    {
        emit clientDisconnect(userClient,userPassword);
    }
}
