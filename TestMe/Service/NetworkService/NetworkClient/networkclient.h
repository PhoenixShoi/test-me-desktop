#ifndef NETWORKCLIENT_H
#define NETWORKCLIENT_H

#include "Object/User/userobject.h"

#include <QObject>
#include <QTcpSocket>
#include <QTcpServer>

class NetworkClient : public QObject
{
    Q_OBJECT
public:
    explicit NetworkClient(QTcpServer *tcpServer, QObject *parent = 0);

private:
    bool flagReady=false;
    QTcpSocket *clientSocket;
    UserObject *userClient;
    QString userPassword="";
    void controlManager(QString command="");
    void checkReady();

signals:
    void clientReady(UserObject *userClient,QString password);
    void clientSendMessage(QString message,UserObject *userClient,QString password);
    void clientDisconnect(UserObject *userClient,QString password);
public slots:
    void slotReadClient();
    void slotDisconnectClient();
};

#endif // NETWORKCLIENT_H
