#ifndef USEROBJECT_H
#define USEROBJECT_H

#include <QString>

class UserObject
{
public:
    UserObject(QString userID="-1",QString name="",QString login="",QString password="");
    QString getID();
    QString getLogin();
    QString getName();
    void setID(QString newID);
    bool checkPassword(QString checkPassword);
private:
    QString userID;
    QString name;
    QString login;
protected:
    QString password;
};

#endif // USEROBJECT_H
