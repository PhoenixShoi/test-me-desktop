#ifndef GROUPOBJECT_H
#define GROUPOBJECT_H

#include <QString>

class GroupObject
{
public:
    GroupObject(QString groupID="-1",QString nameGroup="NotFoundName",QString dateGroup="NotFoundDate",int countPeopleGroup=-1);
    QString getID();
    QString getName();
    QString getDate();
    int getCount();
private:
    QString groupID;
    QString nameGroup;
    QString dateGroup;
    int countPeopleGroup;
};

#endif // GROUPOBJECT_H
