#include "groupobject.h"
#include <QDebug>

GroupObject::GroupObject(QString groupID, QString nameGroup, QString dateGroup, int countPeopleGroup)
{
    this->groupID=groupID;
    this->nameGroup=nameGroup;
    this->dateGroup=dateGroup;
    this->countPeopleGroup=countPeopleGroup;
}

QString GroupObject::getID()
{
    return groupID;
}

QString GroupObject::getName()
{
    return nameGroup;
}

QString GroupObject::getDate()
{
    return dateGroup;
}

int GroupObject::getCount()
{
    return countPeopleGroup;
}
