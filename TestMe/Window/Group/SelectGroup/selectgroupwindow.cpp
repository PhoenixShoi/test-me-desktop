#include "selectgroupwindow.h"
#include "ui_selectgroupwindow.h"
#include "Window/Group/SelectGroup/ItemSelectGroupWindow/itemselectgroupwindow.h"
#include "Service/DatabaseService/databaseservice.h"
#include "Menu/menu.h"
#include "Window/Group/CreateGroup/creategroupwindow.h"
#include "Window/Group/EditGroup/editgroupwindow.h"

#include <vector>
#include <QDebug>

SelectGroupWindow::SelectGroupWindow(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::SelectGroupWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Выбор группы");
    ui->addPushButton->setText("Добавить группу");

    this->startGroupAdded();
}

SelectGroupWindow::~SelectGroupWindow()
{
    delete ui;
}

void SelectGroupWindow::addGroupItem(GroupObject newGroup)
{
    ItemSelectGroupWindow *newItemGroup = new ItemSelectGroupWindow(newGroup);
    connect(newItemGroup,SIGNAL(itemMenuMouseClick()),this,SLOT(itemMouseEventClick()));
    connect(newItemGroup,SIGNAL(deleteButtonFromItem(QString)),this,SLOT(deleteItem(QString)));
    ui->itemsGroupLayout->insertWidget(ui->itemsGroupLayout->count()-1,newItemGroup);
}

void SelectGroupWindow::startGroupAdded()
{
    DatabaseService *baseService = new DatabaseService();
    std::vector<GroupObject> *allGroups = new std::vector<GroupObject>;

    baseService->getAllGroup(allGroups);

    for(int i=0;i<allGroups->size();i++)
    {
        this->addGroupItem(allGroups->at(i));
    }
    delete baseService;
}

void SelectGroupWindow::closeEvent(QCloseEvent *event)
{
    if(!this->flagBackMenu)
    {
        Menu *menu = new Menu();
        menu->show();
    }

    this->close();
}


void SelectGroupWindow::itemMouseEventClick()
{
    flagBackMenu=true;
    this->close();
}

void SelectGroupWindow::deleteItem(QString itemID)
{
    DatabaseService *baseService = new DatabaseService();

    baseService->deleteGroup(itemID);

    delete baseService;
}

void SelectGroupWindow::on_addPushButton_clicked()
{
    CreateGroupWindow *createWindow = new CreateGroupWindow();
    createWindow->show();

    this->flagBackMenu=true;

    this->close();
}
