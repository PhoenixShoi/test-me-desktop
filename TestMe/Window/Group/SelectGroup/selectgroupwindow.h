#ifndef SELECTGROUPWINDOW_H
#define SELECTGROUPWINDOW_H

#include "Object/Group/groupobject.h"
#include "Window/Group/SelectGroup/ItemSelectGroupWindow/itemselectgroupwindow.h"

#include <QFrame>
#include <QCloseEvent>

namespace Ui {
class SelectGroupWindow;
}

class SelectGroupWindow : public QFrame
{
    Q_OBJECT

public:
    explicit SelectGroupWindow(QWidget *parent = 0);
    ~SelectGroupWindow();

private slots:

    void itemMouseEventClick();

    void deleteItem(QString itemID="");

    void on_addPushButton_clicked();

private:
    Ui::SelectGroupWindow *ui;
    void addGroupItem(GroupObject newGroup);
    void startGroupAdded();

    void closeEvent(QCloseEvent *event);
    bool flagBackMenu=false;
};

#endif // SELECTGROUPWINDOW_H
