#include "itemselectgroupwindow.h"
#include "ui_itemselectgroupwindow.h"
#include "Window/Group/EditGroup/editgroupwindow.h"
#include "Window/Group/EditNameGroup/editnamegroup.h"

#include <QMouseEvent>
#include <QDebug>


ItemSelectGroupWindow::ItemSelectGroupWindow(GroupObject grObject, QWidget *parent) :
    QFrame(parent),
    ui(new Ui::ItemSelectGroupWindow)
{
    ui->setupUi(this);
    this->saveObject=grObject;

    ui->peopleLabel->setPixmap(QPixmap(":/icon/peopleIcon").scaled(32,32));
    ui->nameGroupLabel->setText(saveObject.getName());
    ui->infoPeopleLabel->setText(QString::number(saveObject.getCount()));
    ui->dateLabel->setText(saveObject.getDate());

    QPalette pal;
    pal.setColor(QPalette::Background, QColor(232, 233, 232) );
    this->setPalette(pal);
}

ItemSelectGroupWindow::~ItemSelectGroupWindow()
{
    delete ui;
}

void ItemSelectGroupWindow::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        EditGroupWindow *editGroup=new EditGroupWindow(saveObject);
        editGroup->show();
        emit itemMenuMouseClick();
    }
}

void ItemSelectGroupWindow::on_deletePushButton_clicked()
{
    emit deleteButtonFromItem(saveObject.getID());
    this->close();
}

void ItemSelectGroupWindow::on_pushButton_clicked()
{
    EditNameGroup *editNameGroup = new EditNameGroup(this->saveObject,1);
    editNameGroup->show();
    emit itemMenuMouseClick();
}
