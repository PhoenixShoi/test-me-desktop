#ifndef ITEMSELECTGROUPWINDOW_H
#define ITEMSELECTGROUPWINDOW_H

#include "Object/Group/groupobject.h"
#include <QFrame>

namespace Ui {
class ItemSelectGroupWindow;
}

class ItemSelectGroupWindow : public QFrame
{
    Q_OBJECT

public:
    explicit ItemSelectGroupWindow(GroupObject grObject, QWidget *parent = 0);
    ~ItemSelectGroupWindow();

private:
    Ui::ItemSelectGroupWindow *ui;

    GroupObject saveObject;

    virtual void mousePressEvent(QMouseEvent *event);

signals:
    void itemMenuMouseClick();
    void deleteButtonFromItem(QString itemID);

private slots:
    void on_deletePushButton_clicked();
    void on_pushButton_clicked();
};

#endif // ITEMSELECTGROUPWINDOW_H
