#include "editgroupwindow.h"
#include "ui_editgroupwindow.h"
#include "Window/Group/EditGroup/ItemEditWindow/itemeditwindow.h"
#include "Window/Group/SelectGroup/selectgroupwindow.h"
#include "Window/Group/EditNameGroup/editnamegroup.h"


EditGroupWindow::EditGroupWindow(GroupObject editGroup,QWidget *parent) :
    QFrame(parent),
    ui(new Ui::EditGroupWindow)
{
    ui->setupUi(this);

    this->savegroup=editGroup;

    this->groupID = editGroup.getID();
    this->countUserInGroup = editGroup.getCount();
    qDebug()<<this->groupID<<endl;

    this->setWindowTitle("Изменение группы");
    ui->listClientGroupBox->setTitle("Список клиентов");
    ui->infoConnectionLabel->setText("Информация о подключении");

    this->nService = new NetworkService();
    connect(nService,SIGNAL(newClientReady(UserObject*,QString)),this,SLOT(newClient(UserObject*,QString)));
    connect(nService,SIGNAL(newClientMessage(QString,UserObject*,QString)),this,SLOT(newMessage(QString,UserObject*,QString)));
    connect(nService,SIGNAL(clientDisconnect(UserObject*,QString)),this,SLOT(clientDisconnect(UserObject*,QString)));

    if(nService->getServerAddress()!="")
    {
        ui->logLineEdit->setText("IP-адрес сервера: "+nService->getServerAddress());
        ui->reconnectpPushButton->close();
    }
    else
    {
        ui->logLineEdit->setText("Not connection");
    }

    this->startUserAdded();

    ui->infoCountRegisterUserPushButton->setText(QString().number(countUserInGroup));
}

EditGroupWindow::~EditGroupWindow()
{
    delete ui;
}

void EditGroupWindow::startUserAdded()
{
    DatabaseService *baseService = new DatabaseService();

    std::vector<UserObject> *allUsers = new std::vector<UserObject>;

    baseService->getAllUsersFromGroup(allUsers,this->groupID);

    for(int i=0;i<allUsers->size();i++)
    {
        this->addNewItem(allUsers->at(i));
    }
    delete baseService;
}

void EditGroupWindow::addNewItem(UserObject newUserItem)
{
    ItemEditWindow *item= new ItemEditWindow(newUserItem);
    connect(this,SIGNAL(clientStatus(UserObject*,bool)),item,SLOT(status(UserObject*,bool)));
    connect(item,SIGNAL(deleteButtonClicked(QString)),this,SLOT(clientLinkDeleted(QString)));
    ui->listLayout->insertWidget(ui->listLayout->count()-1,item);
}

void EditGroupWindow::closeEvent(QCloseEvent *event)
{
    if(!this->editNameGroupFlag)
    {
        SelectGroupWindow *selectMenu = new SelectGroupWindow();
        selectMenu->show();
    }

    this->close();
}

void EditGroupWindow::newClient(UserObject *client,QString userPassword)
{
    DatabaseService *baseService = new DatabaseService();
    while(!baseService->checkUser(client))
    {
        baseService->addUser(client,userPassword);
    }
    while(!baseService->checkUserInGroup(client->getID(),groupID))
    {
        baseService->createConnectUserAndGroup(client->getID(),groupID);
        this->addNewItem(*client);
        this->countUserInGroup++;
        ui->infoCountRegisterUserPushButton->setText(QString().number(countUserInGroup));
    }
    emit clientStatus(client,true,userPassword);
    qDebug()<<"Edit Window new client"<<endl;
    int countConnected=ui->infoCountConnectionUserPushButton->text().toInt();
    ui->infoCountConnectionUserPushButton->setText(QString().number(countConnected+1));
}

void EditGroupWindow::newMessage(QString message, UserObject *client,QString userPassword)
{
    qDebug()<<"Edit Window new Message"<<endl;
    ui->chatPlainTextEdit->appendPlainText(client->getName()+":"+message);
}

void EditGroupWindow::clientDisconnect(UserObject *client,QString userPassword)
{
    emit clientStatus(client,false);
    qDebug()<<"Edit Window client Disconnect"<<endl;
    int countConnected=ui->infoCountConnectionUserPushButton->text().toInt();
    ui->infoCountConnectionUserPushButton->setText(QString().number(countConnected-1));
}

void EditGroupWindow::clientLinkDeleted(QString deleteClientID)
{
    DatabaseService *baseService = new DatabaseService();
    baseService->deleteConnectUserAndGroup(deleteClientID,groupID);
    this->countUserInGroup--;
    ui->infoCountRegisterUserPushButton->setText(QString().number(countUserInGroup));
}

void EditGroupWindow::on_reconnectpPushButton_clicked()
{
    this->nService= new NetworkService();
    if(nService->getServerAddress()!="")
    {
        ui->logLineEdit->setText("IP-адрес сервера: "+nService->getServerAddress());
        ui->reconnectpPushButton->close();
    }
    else
    {
        ui->logLineEdit->setText("Not connection");
    }
}

void EditGroupWindow::on_editGroupArgumentPushButton_clicked()
{
    EditNameGroup *editName = new EditNameGroup(this->savegroup,0);
    editName->show();
    this->editNameGroupFlag=true;
    this->close();
}
