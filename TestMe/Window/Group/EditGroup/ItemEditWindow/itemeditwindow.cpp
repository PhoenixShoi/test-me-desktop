#include "itemeditwindow.h"
#include "ui_itemeditwindow.h"

#include <QDebug>

ItemEditWindow::ItemEditWindow(UserObject usr,QWidget *parent) :
    QFrame(parent),
    ui(new Ui::ItemEditWindow)
{
    ui->setupUi(this);

    this->userDataItem=usr;

    ui->statusLabel->setPixmap(QPixmap(":/icon/disconnectIcon").scaled(16,16));

    ui->deleteButton->setText("");
    QPixmap pixmap(":/icon/deleteIcon");
    QIcon ButtonIcon(pixmap);
    ui->deleteButton->setIcon(ButtonIcon);
    ui->deleteButton->setIconSize(QSize(16,16));

    ui->infoLabel->setText(userDataItem.getName());

}

ItemEditWindow::~ItemEditWindow()
{
    delete ui;
}

void ItemEditWindow::status(UserObject *usr, bool statusFlag,QString userPassword)
{
    qDebug()<<userDataItem.checkPassword(userPassword)<<endl;
    if((usr->getID()==userDataItem.getID()))
    {
        qDebug()<<usr->getID()<<" Status="<<statusFlag<<endl;
        if(statusFlag)
        {
            ui->statusLabel->setPixmap(QPixmap(":/icon/connectIcon").scaled(16,16));
        }
        else
        {
            ui->statusLabel->setPixmap(QPixmap(":/icon/disconnectIcon").scaled(16,16));
        }
    }
}



void ItemEditWindow::on_deleteButton_clicked()
{
    emit deleteButtonClicked(this->userDataItem.getID());
    this->close();
}
