#ifndef ITEMEDITWINDOW_H
#define ITEMEDITWINDOW_H

#include "Object/User/userobject.h"

#include <QFrame>

namespace Ui {
class ItemEditWindow;
}

class ItemEditWindow : public QFrame
{
    Q_OBJECT

public:
    explicit ItemEditWindow(UserObject usr,QWidget *parent = 0);
    ~ItemEditWindow();

public slots:
    void status(UserObject *usr,bool statusFlag=false,QString userPassword="");

private slots:
    void on_deleteButton_clicked();

private:
    Ui::ItemEditWindow *ui;
    UserObject userDataItem;

signals:
    void deleteButtonClicked(QString deleteClientID);
};

#endif // ITEMEDITWINDOW_H
