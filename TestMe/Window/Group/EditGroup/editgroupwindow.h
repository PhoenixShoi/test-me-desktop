#ifndef EDITGROUPWINDOW_H
#define EDITGROUPWINDOW_H

#include "Service/NetworkService/networkservice.h"
#include "Service/DatabaseService/databaseservice.h"
#include "Object/Group/groupobject.h"

#include <QFrame>
#include <QCloseEvent>


namespace Ui {
class EditGroupWindow;
}

class EditGroupWindow : public QFrame
{
    Q_OBJECT

public:
    explicit EditGroupWindow(GroupObject editGroup,QWidget *parent = 0);
    ~EditGroupWindow();

private:
    Ui::EditGroupWindow *ui;
    NetworkService *nService;
    QString groupID="";
    int countUserInGroup=0;
    void startUserAdded();
    void addNewItem(UserObject newUserItem);
    void closeEvent(QCloseEvent *event);
    GroupObject savegroup;
    bool editNameGroupFlag=false;
signals:
    void clientStatus(UserObject *client,bool flagStatus=false,QString userPassword="");
public slots:
    void newClient(UserObject *client,QString userPassword);
    void newMessage(QString message,UserObject *client,QString userPassword);
    void clientDisconnect(UserObject *client,QString userPassword);
    void clientLinkDeleted(QString deleteClientID);
private slots:
    void on_reconnectpPushButton_clicked();
    void on_editGroupArgumentPushButton_clicked();
};

#endif // EDITGROUPWINDOW_H
