#ifndef CREATEGROUPWINDOW_H
#define CREATEGROUPWINDOW_H

#include <QFrame>
#include <QObject>
#include <QCloseEvent>

namespace Ui {
class CreateGroupWindow;
}

class CreateGroupWindow : public QFrame
{
    Q_OBJECT

public:
    explicit CreateGroupWindow(QWidget *parent = 0);
    ~CreateGroupWindow();

private slots:
    void on_createGroupButton_clicked();

private:
    Ui::CreateGroupWindow *ui;
    void closeEvent(QCloseEvent *event);
};

#endif // CREATEGROUPWINDOW_H
