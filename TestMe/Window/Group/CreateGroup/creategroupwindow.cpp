#include "creategroupwindow.h"
#include "ui_creategroupwindow.h"
#include "Service/DatabaseService/databaseservice.h"
#include "Object/Group/groupobject.h"
#include "Window/Group/SelectGroup/selectgroupwindow.h"


#include <QDateTime>

CreateGroupWindow::CreateGroupWindow(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::CreateGroupWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Создание новой группы");
    ui->createGroupButton->setText("Создать");
    ui->inputNameLabel->setText("Имя группы");
    ui->inputNameLineEdit->setPlaceholderText("Введите имя группы");
}

CreateGroupWindow::~CreateGroupWindow()
{
    delete ui;
}

void CreateGroupWindow::on_createGroupButton_clicked()
{
    DatabaseService *dbService = new DatabaseService();
    dbService->addGroup(GroupObject("-1",ui->inputNameLineEdit->text(),QDateTime::currentDateTime().toString(Qt::SystemLocaleDate),0));
    delete dbService;

    this->close();
}

void CreateGroupWindow::closeEvent(QCloseEvent *event)
{
    SelectGroupWindow *selectMenu = new SelectGroupWindow();
    selectMenu->show();

    this->close();
}
