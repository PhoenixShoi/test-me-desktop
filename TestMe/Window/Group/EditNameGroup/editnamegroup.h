#ifndef EDITNAMEGROUP_H
#define EDITNAMEGROUP_H

#include "Object/Group/groupobject.h"

#include <QFrame>
#include <QCloseEvent>

namespace Ui {
class EditNameGroup;
}

class EditNameGroup : public QFrame
{
    Q_OBJECT

public:
    explicit EditNameGroup(GroupObject editGroup,int typeEdit,QWidget *parent = 0);
    ~EditNameGroup();

private slots:
    void on_selectPushButton_clicked();

private:
    Ui::EditNameGroup *ui;
    void closeEvent(QCloseEvent *event);
    GroupObject editGroup;
    int typeEdit=0;
};

#endif // EDITNAMEGROUP_H
