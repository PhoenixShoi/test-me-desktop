#include "editnamegroup.h"
#include "ui_editnamegroup.h"

#include "Service/DatabaseService/databaseservice.h"
#include "Window/Group/SelectGroup/selectgroupwindow.h"
#include "Window/Group/EditGroup/editgroupwindow.h"

EditNameGroup::EditNameGroup(GroupObject editGroup,int typeEdit,QWidget *parent) :
    QFrame(parent),
    ui(new Ui::EditNameGroup)
{
    ui->setupUi(this);
    this->setWindowTitle("Изменить имя группы");
    this->editGroup=editGroup;
    this->typeEdit=typeEdit;

    ui->selectPushButton->setText("Выбрать");
}

EditNameGroup::~EditNameGroup()
{
    delete ui;
}

void EditNameGroup::on_selectPushButton_clicked()
{
    if(ui->nameLineEdit->text()!="")
    {
        DatabaseService *baseService = new DatabaseService();
        baseService->editGroup(editGroup.getID(),ui->nameLineEdit->text());
        this->close();
    }
}

void EditNameGroup::closeEvent(QCloseEvent *event)
{
    if(typeEdit)
    {
        SelectGroupWindow *selectGroup = new SelectGroupWindow();
        selectGroup->show();
    }
    else
    {
        EditGroupWindow *editGroupWindow = new EditGroupWindow(editGroup);
        editGroupWindow->show();
    }
    this->close();
}
