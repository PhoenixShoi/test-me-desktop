#ifndef SETTINGWINDOW_H
#define SETTINGWINDOW_H

#include <QFrame>
#include <QCloseEvent>

namespace Ui {
class SettingWindow;
}

class SettingWindow : public QFrame
{
    Q_OBJECT

public:
    explicit SettingWindow(QWidget *parent = 0);
    ~SettingWindow();

private slots:
    void on_generalButton_clicked();

    void on_networkButton_clicked();

    void on_splashScreenCheckBox_clicked(bool checked);

    void on_applicationIconComboBox_activated(int index);

    void on_portLineEdit_editingFinished();

    void on_defaultSettingsButton_clicked();

private:
    Ui::SettingWindow *ui;
    void closeEvent(QCloseEvent *event);
};

#endif // SETTINGWINDOW_H
