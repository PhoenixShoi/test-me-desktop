#include "settingwindow.h"
#include "ui_settingwindow.h"

#include "Service/NetworkService/networkservice.h"
#include "Menu/menu.h"

#include <QSettings>
#include <QDebug>
#include <QMessageBox>


SettingWindow::SettingWindow(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::SettingWindow)
{
    ui->setupUi(this);

    QSettings setting;

    this->setWindowTitle("Настройки");
    ui->generalButton->setText("Основные");
    ui->networkButton->setText("Сеть");
    ui->applicationIconLabel->setText("Стиль иконки");
    ui->splashScreenCheckBox->setText("Включить заставку");
    ui->startingGroupBox->setTitle("Запуск программы");
    ui->splashScreenCheckBox->setChecked(setting.value("/General/EnableSpashScreen",true).toBool());

    for(int i=0;i<ui->applicationIconComboBox->count();i++)
    {
        ui->applicationIconComboBox->setItemText(i,"Стиль "+QString::number(i+1));
    }
    ui->applicationIconComboBox->setCurrentIndex(setting.value("/General/ApplicationIcon",1).toInt()-1);

    ui->portLineEdit->setText(setting.value("/Network/Port","").toString());

    ui->portLabel->setText("Порт");

    ui->defaultSettingsButton->setText("По умолчанию");

    QRegExp rxPort("^[0-9]+$");
    QRegExpValidator *validatorPort = new QRegExpValidator(QRegExp(rxPort),ui->portLineEdit);
    ui->portLineEdit->setValidator(validatorPort);

    ui->stackedWidget->setCurrentWidget(ui->generalPage);
}

SettingWindow::~SettingWindow()
{
    QSettings setting;
    if(setting.value("/Network/Port","").toString()=="")
    {
        setting.setValue("/Network/Port",2323);
    }
    delete ui;
}

void SettingWindow::on_generalButton_clicked()
{
    ui->stackedWidget->setCurrentWidget(ui->generalPage);
}

void SettingWindow::on_networkButton_clicked()
{
    ui->stackedWidget->setCurrentWidget(ui->networkPage);
}

void SettingWindow::on_splashScreenCheckBox_clicked(bool checked)
{
    QSettings setting;
    qDebug()<<setting.value("/General/EnableSpashScreen",true).toBool()<<endl;
    setting.setValue("/General/EnableSpashScreen",checked);
}

void SettingWindow::on_applicationIconComboBox_activated(int index)
{
    QSettings setting;
    setting.setValue("/General/ApplicationIcon",index+1);
    QIcon icon(":/icon/testMeStyle"+setting.value("/General/ApplicationIcon",1).toString()+"Icon");
    QApplication::setWindowIcon(icon);
}

void SettingWindow::on_portLineEdit_editingFinished()
{
    QSettings setting;
    setting.setValue("/Network/Port",ui->portLineEdit->text());
}

void SettingWindow::on_defaultSettingsButton_clicked()
{
    this->hide();
    if(QMessageBox::question(this,"Настройки по умолчанию","Восстановить настройки по умолчанию?","Да","Нет")==0)
    {
        QSettings setting;
        setting.remove("/General/EnableSpashScreen");
        setting.remove("/General/ApplicationIcon");
        setting.remove("/Network/Port");

        ui->splashScreenCheckBox->setChecked(setting.value("/General/EnableSpashScreen",true).toBool());
        ui->applicationIconComboBox->setCurrentIndex(setting.value("/General/ApplicationIcon",1).toInt()-1);
        ui->portLineEdit->setText(setting.value("/Network/Port","").toString());

        QIcon icon(":/icon/testMeStyle"+setting.value("/General/ApplicationIcon",1).toString()+"Icon");
        QApplication::setWindowIcon(icon);
    }
    this->show();
}

void SettingWindow::closeEvent(QCloseEvent *event)
{
    Menu *menu = new Menu();
    menu->show();

    this->close();
}
