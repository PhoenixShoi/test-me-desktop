#include "itemmenu.h"
#include "ui_itemmenu.h"

#include <QMouseEvent>
#include <QDebug>

ItemMenu::ItemMenu(QString infoIconText,QString systemName,QWidget *parent) :
    QFrame(parent),
    ui(new Ui::ItemMenu)
{
    ui->setupUi(this);
    this->infoIconText=infoIconText;
    this->systemName=systemName;
    ui->infoLabel->setText(infoIconText);
    ui->iconLabel->setPixmap(QPixmap(":/icon/"+systemName+"Icon"));
}

ItemMenu::~ItemMenu()
{
    delete ui;
}

void ItemMenu::enterEvent(QEvent *event)
{
    emit itemMenuMouseSignal(systemName);
}

void ItemMenu::leaveEvent(QEvent *event)
{
    emit itemMenuMouseSignal("none");
}

void ItemMenu::mousePressEvent(QMouseEvent *event)
{
    if ( event->button() == Qt::LeftButton )
    {
        emit itemMenuMouseClick(this->systemName);
    }
}
