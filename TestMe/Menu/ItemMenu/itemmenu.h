#ifndef ITEMMENU_H
#define ITEMMENU_H

#include <QFrame>

namespace Ui {
class ItemMenu;
}

class ItemMenu : public QFrame
{
    Q_OBJECT

public:
    explicit ItemMenu(QString infoIconText="",QString systemName="",QWidget *parent = 0);
    ~ItemMenu();

private:
    Ui::ItemMenu *ui;

    QString infoIconText="";//Название пункта для меню
    QString systemName="";//Название пункта в системе

    virtual void enterEvent (QEvent *event);//Отлов нахождения мыши на поле меню
    virtual void leaveEvent (QEvent *event);//Отлов ухода мыши из поля меню
    virtual void mousePressEvent(QMouseEvent *event);

signals:
    void itemMenuMouseSignal(QString actionItemMenu);//Сигнал что мышь или ушла или пришла с элемента
    void itemMenuMouseClick(QString actionItemMenu);
};

#endif // ITEMMENU_H
