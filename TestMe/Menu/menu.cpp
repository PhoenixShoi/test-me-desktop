#include "menu.h"
#include "ui_menu.h"

#include "Menu/ItemMenu/itemmenu.h"
#include "Window/Setting/settingwindow.h"
#include "Window/Group/SelectGroup/selectgroupwindow.h"


#include <QPainter>

Menu::Menu(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Menu)
{
    ui->setupUi(this);

    this->setWindowTitle(QString("Меню"));
    this->selectItem("default");
    this->loadingItem();

}

Menu::~Menu()
{
    delete ui;
}

void Menu::addItemMenu(QString infoItem, QString systemNameItem)
{
    ItemMenu *newItemMenu = new ItemMenu(infoItem,systemNameItem);
    connect(newItemMenu,SIGNAL(itemMenuMouseSignal(QString)),this,SLOT(itemMouseEvent(QString)));
    connect(newItemMenu,SIGNAL(itemMenuMouseClick(QString)),this,SLOT(itemMouseEventClick(QString)));
    ui->menuLayout->addWidget(newItemMenu);
}

void Menu::selectItem(QString systemNameItem)
{
    QPixmap reviewBackground(":/background/"+systemNameItem+"Background");

    ui->reviewLabel->setPixmap(reviewBackground);
}

void Menu::loadingItem()
{
    addItemMenu("Запуск теста","playTest");
    addItemMenu("Редактор тестов","editTest");
    addItemMenu("Редактор групп","editGroup");
    addItemMenu("Настройки","setting");
    addItemMenu("Выход","exit");
}

void Menu::itemMouseEvent(QString value)
{
    if(value!="none")
    {
        this->selectItem(value);
    }
    else
    {
        this->selectItem("default");
    }
}

void Menu::itemMouseEventClick(QString systemName)
{
    if(systemName=="playTest")
    {
        this->close();
        return;
    }
    if(systemName=="editTest")
    {
        this->close();
        return;
    }
    if(systemName=="editGroup")
    {
        SelectGroupWindow *selectGroup = new SelectGroupWindow();
        selectGroup->show();
        this->close();
        return;
    }
    if(systemName=="setting")
    {
        SettingWindow *setting = new SettingWindow();
        setting->show();
        this->close();
        return;
    }
    if(systemName=="exit")
    {
        this->close();
        return;
    }

}
