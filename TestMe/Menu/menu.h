#ifndef MENU_H
#define MENU_H

#include <QMainWindow>

namespace Ui {
class Menu;
}

class Menu : public QMainWindow
{
    Q_OBJECT

public:
    explicit Menu(QWidget *parent = 0);
    ~Menu();

private:
    Ui::Menu *ui;
    void addItemMenu(QString infoItem,QString systemNameItem);
    void selectItem(QString systemNameItem);
    void loadingItem();

public slots:
    void itemMouseEvent(QString value);
    void itemMouseEventClick(QString systemName);
};

#endif // MENU_H
