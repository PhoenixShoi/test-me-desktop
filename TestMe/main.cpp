#include "Menu/menu.h"

#include <QSettings>
#include <QApplication>
#include <QSplashScreen>
#include <QDebug>

void checkAllSetting()
{
    QSettings setting;
    qDebug()<<"/General/EnableSpashScreen="<<setting.value("/General/EnableSpashScreen",true).toBool()<<endl;
    qDebug()<<"/General/ApplicationIcon="<<setting.value("/General/ApplicationIcon","Style1").toString()<<endl;
    qDebug()<<"/Network/Port="<<setting.value("/Network/Port","").toString()<<endl;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QCoreApplication::setOrganizationName("Infinity Team");
    QCoreApplication::setApplicationName("Test Me");
    QCoreApplication::setApplicationVersion("0.0.1");

    checkAllSetting();

    QSettings setting;

    Menu menu;
    QIcon icon(":/icon/testMeStyle"+setting.value("/General/ApplicationIcon",1).toString()+"Icon");
    QApplication::setWindowIcon(icon);
    if(setting.value("/General/EnableSpashScreen",true).toBool())
    {
        QSplashScreen *splash = new QSplashScreen;
        splash->setPixmap(QPixmap(":/images/splash").scaled(QSize(500,500),  Qt::KeepAspectRatio));
        splash->show();
        menu.show();
        splash->finish(&menu);
        delete splash;
    }
    else
    {
        menu.show();
    }

    return a.exec();
}
