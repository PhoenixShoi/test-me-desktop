#-------------------------------------------------
#
# Project created by QtCreator 2016-02-07T00:05:32
#
#-------------------------------------------------

QT       += core gui sql network
CONFIG   +=c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TestMe
TEMPLATE = app



SOURCES += main.cpp\
    Menu/ItemMenu/itemmenu.cpp \
    Menu/menu.cpp \
    Service/DatabaseService/databaseservice.cpp \
    Object/Group/groupobject.cpp \
    Object/User/userobject.cpp \
    Window/Group/SelectGroup/selectgroupwindow.cpp \
    Window/Group/SelectGroup/ItemSelectGroupWindow/itemselectgroupwindow.cpp \
    Window/Group/CreateGroup/creategroupwindow.cpp \
    Window/Group/EditGroup/editgroupwindow.cpp \
    Window/Group/EditGroup/ItemEditWindow/itemeditwindow.cpp \
    Service/NetworkService/networkservice.cpp \
    Service/NetworkService/NetworkClient/networkclient.cpp \
    Window/Setting/settingwindow.cpp \
    Window/Group/EditNameGroup/editnamegroup.cpp


HEADERS  += \
    Menu/ItemMenu/itemmenu.h \
    Menu/menu.h \
    Service/DatabaseService/databaseservice.h \
    Object/Group/groupobject.h \
    Object/User/userobject.h \
    Window/Group/SelectGroup/selectgroupwindow.h \
    Window/Group/SelectGroup/ItemSelectGroupWindow/itemselectgroupwindow.h \
    Window/Group/CreateGroup/creategroupwindow.h \
    Window/Group/EditGroup/editgroupwindow.h \
    Window/Group/EditGroup/ItemEditWindow/itemeditwindow.h \
    Service/NetworkService/networkservice.h \
    Service/NetworkService/NetworkClient/networkclient.h \
    Window/Setting/settingwindow.h \
    Window/Group/EditNameGroup/editnamegroup.h

FORMS    += \
    Menu/menu.ui \
    Menu/ItemMenu/itemmenu.ui \
    Window/Setting/settingwindow.ui \
    Window/Group/SelectGroup/selectgroupwindow.ui \
    Window/Group/SelectGroup/ItemSelectGroupWindow/itemselectgroupwindow.ui \
    Window/Group/CreateGroup/creategroupwindow.ui \
    Window/Group/EditGroup/editgroupwindow.ui \
    Window/Group/EditGroup/ItemEditWindow/itemeditwindow.ui \
    Window/Group/EditNameGroup/editnamegroup.ui

RESOURCES += \
    res.qrc
